'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Roles',
      [
        {
          id: 1,
          name: 'user',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 2,
          name: 'admin',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 3,
          name: 'manager',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
