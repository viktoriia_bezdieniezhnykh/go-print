'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert(
      'Printers',
      [
        {
          name: 'Printer 1',
          uri: 'ipp://49.12.14.139:631//printers/Printer1',
          status: 'active',
          address: 'г. Киев, ул.К.Малевича, 86П.',
          latlng: `{
            "latitude": 50.448893,
            "longitude": 30.530512
          }`,
          description: 'Бизнес-центр "Lybid"',
          region: 'ua'
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  },
};
