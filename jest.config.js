module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleNameMapper: {
    "@goprint/(.*)": "<rootDir>/src/$1"
  },
};