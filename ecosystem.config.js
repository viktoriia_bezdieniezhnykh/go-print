module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [
        {
            name: 'backend',
            script: '/app/dist/index.js',
            watch: false
        }
    ]
};
