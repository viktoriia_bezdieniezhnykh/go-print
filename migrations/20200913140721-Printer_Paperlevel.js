'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.addColumn('Printers', 'paperLevel', Sequelize.JSON);
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeColumn('Printers', 'paperLevel');
  },
};
