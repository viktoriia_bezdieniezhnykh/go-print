'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.addColumn('Printers', 'inkLevel', Sequelize.JSON);
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeColumn('Printers', 'inkLevel');
  },
};
