require('newrelic');
require('dotenv').load();

import * as socketIo from 'socket.io';
import { app } from './app';

import { dbConnect } from './db';
import { setTasks } from './utils/tasks';
import { logger } from './utils/logger';
import { CCardEvents } from './events/creditcard.events';
import { PaymentEvents } from './events/payment.events';
import { TaskEvents } from './events/task.events';


const redisOptions = {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASS,
};

dbConnect().then(() => logger.info('DB ready'));
setTasks().then(() => logger.info('Tasks ready'));

const server = app.listen(process.env.PORT, () => {
    console.log(
        'App is running at http://localhost:%d in %s mode',
        process.env.PORT,
        process.env.NODE_ENV
    );
});

const io = socketIo(server);

const cardsEvents = new CCardEvents(io);
const taskEvents = new TaskEvents(io);
const paymentEvents = new PaymentEvents(io);

app.set('cardsEvents', cardsEvents);
app.set('taskEvents', taskEvents);
app.set('paymentEvents', paymentEvents);

export { app, server };
