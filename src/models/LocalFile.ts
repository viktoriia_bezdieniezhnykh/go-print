import {
  AutoIncrement,
  Column,
  ForeignKey,
  HasMany,
  HasOne,
  Model,
  PrimaryKey,
  Table,
  BelongsTo,
  Default,
  AfterDestroy,
  AfterBulkDestroy,
} from 'sequelize-typescript';
import { User } from './User';
import { Task } from './Task';
import { unlink, unlinkSync } from 'fs';

@Table
export class LocalFile extends Model<LocalFile> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @Column
  originalname: string;

  @Column
  encoding: string;

  @Column
  mimetype: string;

  @Column
  destination: string;

  @Column
  filename: string;

  @Column
  path: string;

  @Column
  size: string;

  @Column
  printerJobId: number;

  @Column
  pages: number;

  @Default(false)
  @Column
  errored: boolean;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @ForeignKey(() => Task)
  @Column
  taskId: number;

  @BelongsTo(() => Task)
  task: Task;

  @AfterDestroy
  static afterDestroyHook(file: LocalFile, options: any) {
    unlinkSync(file.path);
  }
}
