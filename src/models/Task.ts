import { readFileSync } from 'fs';
import * as ipp from 'ipp';
import {
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  Default,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import { logger } from '../utils/logger'; // '../server';
import {
  attributesFromTask,
  sendDataToPrinter,
  getJobAttributes, cancelJob
} from '../utils/cups-util';
import { LocalFile } from './LocalFile';
import { Printer } from './Printer';
import { User } from './User';

@Table
export class Task extends Model<Task> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  // init; payed; printing; ready; errored; canceled
  @Default('init')
  @Column({
    type: DataType.ENUM,
    values: ['init', 'payed', 'printing', 'complete', 'expired', 'errored', 'canceled'],
  })
  status: string;

  @HasMany(() => LocalFile)
  files: LocalFile[];

  @Column
  pagesQty: number;

  @Column(DataType.JSON)
  printConfig: any;

  @Default(false)
  @Column
  payed: boolean;

  @Default(false)
  @Column
  refunded: boolean;

  @Column(DataType.FLOAT)
  cost: number;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @ForeignKey(() => Printer)
  @Column
  printerId: number;

  @BelongsTo(() => Printer)
  printer: Printer;

  @Column
  payedWith: string;

  @Column
  source: string;

  @Default(false)
  @Column
  archived: boolean;

  @Default(false)
  @Column
  test: boolean;

  /**
   * pay
   */
  public async pay(payedWith = 'card') {
    this.payed = true;
    this.status = 'payed';
    this.payedWith = payedWith;
    await this.save();
  }

  /**
   * refund
   */
  public async refund() {
    this.payed = false;
    await this.save();
  }


  /**
   * print
   */
  public async print() {
    let reason = 'ok';

    if (this.status === 'printing') {
      throw Error('Already in work');
    }

    const printer: any = await this.$get('printer');
    const printerInst = ipp.Printer(printer.uri);

    const files: any[] = await this.$get('files');
    if (!files.length) {
      throw new Error('No valid files in task');
    }
    // add all files to queue
    const printAttr = attributesFromTask(this.printConfig);

    const jobs = [];
    for (const file of files) {
      if (!file.errored) {
        if (file.printerJobId) {
          // check job
          try {
            const job = await getJobAttributes(printerInst, file.printerJobId);
            if (job['job-attributes-tag']['job-state'] === 'pending') {
              // skip file
              reason = 'jobExist';
              continue;
            }
          } catch (error) {
            logger.error(error)
          }
        }
        const data = readFileSync(file.path);

        try {
          const job = await sendDataToPrinter(
            printerInst,
            data,
            printAttr,
            file.mimetype,
            `print_job_${this.id}__file_${file.id}`,
            file.filename
          );
          logger.info('printer response', { data: job });
          const jobId = job['job-attributes-tag']['job-id'];

          file.printerJobId = jobId;
          await file.save();

          job['file'] = file;

          jobs.push(job);
        } catch (error) {
          logger.error(error)
          reason = 'errorOnJobCreation';

          // file.errored = true;
          // file.save();
          
          throw error;
        }
      } else {
        reason = 'filesNotValid';
        throw new Error('No valid files in task');
      }
    }
    if (jobs.length) {
      this.status = 'printing';
      await this.save();
    }

    logger.info(`print jobs ${jobs.length}`);

    return { jobs, reason };
  }

  /**
   * expire
   */
  public async expire() {
    const deleteFiles = await LocalFile.destroy({ where: { taskId: this.id } });
    if (this.payed) {
      this.status = 'expired';

      const user = await User.findByPk(this.userId);
      let newBalance = Number(this.cost);
      if (user.balance) {
        newBalance += Number(user.balance);
      }
      await user.update({ balance: newBalance });

      this.refunded = true;
      await this.save();
    } else {
      await this.destroy();
    }
  }

  public async cancel() {
    this.status = 'canceled';
    await this.save();

    const files: any[] = await this.$get('files');
    if (!files.length) {
      throw new Error('No valid files in task');
    }

    const printer: any = await this.$get('printer');
    const printerInst = ipp.Printer(printer.uri);

    for (const file of files) {
      if (!file.errored) {
        if (file.printerJobId) {
          try {
            const job = await cancelJob(printerInst, file.printerJobId);
            logger.info('printer cancelation response', { data: job });
          } catch (error) {
            logger.error(error);
          }
        }
      }
    }
  }
}
