import * as ipp from 'ipp';
import {
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  Unique,
} from 'sequelize-typescript';

import * as querystring from 'querystring';

import { getPrinterAttributes } from '../utils/cups-util';
import { sendMessage } from '../utils/TelegramApi';

import { User } from './User';
import { logger } from '../utils/logger'; // '../server';

interface LatLng {
  latitude: number;
  longitude: number;
}

@Table({ timestamps: false })
export class Printer extends Model<Printer> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @Unique
  @Column
  uri: string;

  @Default('active')
  @Column({
    comment: 'new || active || stopped || idle',
    type: DataType.ENUM('new', 'active', 'stopped', 'idle'),
  })
  status: string;

  @Column({
    comment: 'idle || working || paused || stopped'
  })
  state: string;

  @Column
  location: string;

  @Column(DataType.TEXT)
  address: string;

  @Column(DataType.JSON)
  latlng: LatLng;

  @Column(DataType.TEXT)
  description: string;

  @Default('ua')
  @Column
  region: string;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @Column(DataType.JSON)
  inkLevel: any;

  @Column(DataType.JSON)
  paperLevel: any;

  async checkState() {
    try {
      const printerInst = ipp.Printer(this.uri);
      const printerAttr = await getPrinterAttributes(printerInst);

      if (printerAttr['printer-attributes-tag']['marker-levels']) {
        try {
          const inkLevel = [];
          for (let index = 0; index < printerAttr['printer-attributes-tag']['marker-levels'].length; index++) {
            const name = printerAttr['printer-attributes-tag']['marker-names'][index];
            const level = printerAttr['printer-attributes-tag']['marker-levels'][index];
            inkLevel.push({ name, level });
          }
          this.inkLevel = inkLevel;
        } catch ({ message }) {
          if (!message.startWith('Cannot read property')) {
            logger.error(`Error on ink fetch ${message}`);
          }
        }
      }

      if (printerAttr['printer-attributes-tag']['printer-input-tray']) {
        try {
          const paperLevel = [];
          for (let index = 0; index < printerAttr['printer-attributes-tag']['printer-input-tray'].length; index++) {
            const { name, level } = querystring.parse(printerAttr['printer-attributes-tag']['printer-input-tray'][index], ';');
            paperLevel.push({ name, level });
          }
          this.paperLevel = paperLevel;
        } catch ({ message }) {
          logger.error(`Error on paper fetch ${message}`);
        }
      }


      this.state = printerAttr['printer-attributes-tag']['printer-state'];
      await this.save();
    } catch (error) {
      this.state = 'errored';
      await this.save();
    }
  }

  async changeStatus(state) {
    try {
      if (this.state !== state) {
        this.state = state;
        await this.save();

        await sendMessage(process.env.TELEGRAM_TOKEN, {
          chat_id: process.env.TELEGRAM_CHAT,
          text: `${this.name} - ${this.address} изменение состояния на ${this.state}`,
        });
      }
    } catch ({ message }) {
      logger.error(`changeStatus error ${message}`);
    }
  }
}
