import { AutoIncrement, Column, Model, PrimaryKey, Table } from 'sequelize-typescript';

@Table
export class Code extends Model<Code> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  phone: string;

  @Column
  code: string;

  @Column
  used: boolean;
}
