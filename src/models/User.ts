import * as crypto from 'crypto';
import {
  AutoIncrement,
  BeforeCreate,
  BeforeUpdate,
  BelongsTo,
  Column,
  ForeignKey,
  IsEmail,
  Model,
  PrimaryKey,
  Table,
  Unique,
  HasMany,
  Default,
  DataType,
  AfterCreate,
} from 'sequelize-typescript';

import { EmailSender } from '../utils/email_sender';
import { Role } from './Role';
import { LocalFile } from './LocalFile';
import { Task } from './Task';
import { CreditCard } from './CreditCard';
import { logger } from '../utils/logger'; // '../server';

@Table
export class User extends Model<User> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @IsEmail
  @Unique
  @Column
  email: string;

  @Column
  phone: string;

  @Column
  name: string;

  @Column
  image: string;

  @Column
  hash: string;

  @ForeignKey(() => Role)
  @Column
  roleId: number;

  @BelongsTo(() => Role)
  role: Role;

  @Column
  lastSeen: Date;

  @Column
  resetToken: string;

  @Column
  resetDate: Date;

  @Default(0)
  @Column(DataType.DECIMAL(10, 2))
  balance: number;

  @HasMany(() => LocalFile)
  files: LocalFile[];

  @HasMany(() => Task)
  tasks: Task[];

  @HasMany(() => CreditCard)
  cards: CreditCard;

  @Column
  phoneConfirmed: boolean;

  @AfterCreate
  static async createPassHash(user: User, options: any) {
    try {
      if (user.hash) {
        new EmailSender()
          .sendEmail({
            to: user.email,
            subject: `[GO Print] Регистрация на сайте`,
            template: 'newUser',
            variables: {
              login: user.email,
            },
          })
          .then(res => logger.info('EmailSender', { data: res }))
          .catch((error) => logger.error(error));
      } else {
        const password = crypto.randomBytes(8).toString('hex');
        await user.update({ resetToken: password, resetDate: new Date() });
        new EmailSender().sendEmail({
          to: user.email,
          subject: `[GO Print] Регистрация на сайте`,
          template: 'fbUserPassword',
          variables: {
            login: user.email,
            link: `${process.env.FRONTEND}?t=${password}`,
          },
        })
          .then(res => logger.info('EmailSender', { data: res }))
          .catch((error) => logger.error(error));
      }
    } catch (error) {
      logger.error(error)
    }
  }

  @BeforeUpdate
  static async updatePassword(user: User) {
    try {
      if (user['_previousDataValues']['hash'] !== user.hash) {
        const hash = crypto
          .createHash('md5')
          .update(`${user.hash}|${process.env.SALT}|pass`)
          .digest('hex');

        user.hash = hash;
      }
    } catch (error) { }
  }

  async generatePass() {
    try {
      const password = crypto
        .randomBytes(6)
        .toString('hex')
        .slice(6);

      this.hash = password;
      await this.save();

      new EmailSender().sendEmail({
        to: this.email,
        subject: `[GO Print] Новий пароль`,
        template: 'newPassword',
        variables: {
          password,
          login: this.email,
        },
      })
        .then(res => logger.info('EmailSender', { data: res }))
        .catch((error) => logger.error(error));
      return password;
    } catch (error) {
      logger.error(error);
      throw new Error('Error on send');
    }
  }

  async sendResetLink() {
    try {
      const password = crypto.randomBytes(8).toString('hex');

      this.resetToken = password;
      this.resetDate = new Date();
      await this.save();

      new EmailSender().sendEmail({
        to: this.email,
        subject: `[GO Print] Відновлення паролю`,
        template: 'resetPassword',
        variables: {
          link: `${process.env.FRONT}/reset?t=${password}`,
        },
      })
        .then(res => logger.info('EmailSender', { data: res }))
        .catch((error) => logger.error(error));
      return password;
    } catch (error) {
      logger.error(error)
      throw new Error('Error on send');
    }
  }
}
