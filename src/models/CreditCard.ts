import {
    AutoIncrement,
    Column,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
    BelongsTo,
    Default,
    BeforeCreate,
    BeforeUpdate,
} from 'sequelize-typescript';
import { logger } from '../utils/logger'; // '../server';
import { User } from './User';

@Table
export class CreditCard extends Model<CreditCard> {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @Column
    cardToken: string;

    @Column
    cardName: string;

    @Column
    cardMask: string;

    @Column
    bin: string;

    @Column
    pan: string;

    @Column
    rcId: string;
    @Column
    rcToken: string;

    @Column
    type: string; // visa/mastercard

    @Default(false)
    @Column
    default: boolean;

    @Default(true)
    @Column
    active: boolean;

    @ForeignKey(() => User)
    @Column
    userId: number;

    @BelongsTo(() => User)
    user: User;

    @BeforeCreate
    static async createMask(card: CreditCard) {
        try {
            card.bin = String(card.cardMask).slice(0, 6);
            card.pan = String(card.cardMask).slice(-4);
            card.cardMask =
                String(card.cardMask).slice(0, 4) +
                String('*').repeat(8) +
                String(card.cardMask).slice(-4);
        } catch (error) {
            logger.error(error);
        }
    }

    @BeforeUpdate
    static async updateMask(card: CreditCard) {
        try {
            card.bin = String(card.cardMask).slice(0, 6);
            card.pan = String(card.cardMask).slice(-4);
            card.cardMask =
                String(card.cardMask).slice(0, 4) +
                String('*').repeat(8) +
                String(card.cardMask).slice(-4);
        } catch (error) {
            logger.error(error);
        }
    }
}
