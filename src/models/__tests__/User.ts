import { app } from '@goprint/app';
import { getSeqlize, getModel } from '@goprint/db';
import * as faker from 'faker';

import { authUser, createUser } from "@goprint/utils/auth";

describe("test user model", () => {
    // Set the db object to a variable which can be accessed throughout the whole test file
    let thisDb: any = getSeqlize()
    let curUser;

    const randomString = faker.random.alphaNumeric(10);
    const email = `user-${randomString}@email.com`;
    const password = `password`;

    // Before any tests run, clear the DB and run migrations with Sequelize sync()
    beforeAll(async () => {
        await thisDb.sync({ force: true })
        let { user } = await createUser({ email, password })
        curUser = user;
    })

    it("should succeed when password generated", async () => {
        const password = await curUser.generatePass();

        expect(typeof password).toBe('string');
        expect(String(password).length).toBe(6);
    })

    it("should succeed when token generated", async () => {
        const password = await curUser.sendResetLink();

        expect(typeof password).toBe('string');
        expect(String(password).length).toBe(16);
    })

    // After all tersts have finished, close the DB connection
    afterAll(async (done) => {
        await thisDb.close();
        done();
    })
})
