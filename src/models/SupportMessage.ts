import {
  AfterCreate,
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import { EmailSender } from '../utils/email_sender';
import { User } from './User';

@Table
export class SupportMessage extends Model<SupportMessage> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @Column
  phone: string;

  @Column
  email: string;

  @Column
  status: string;

  @Column(DataType.TEXT)
  message: string;

  @ForeignKey(() => User)
  @AllowNull(true)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @AfterCreate
  static async messageCreated(message: SupportMessage) {
    
    new EmailSender()
      .sendEmail({
        to: process.env.SUPPORT_EMAIL,
        subject: `Повідомлення до служби підтримки`,
        template: 'supportMessage',
        variables: {
          message: message.message,
          name: message.name,
          email: message.email,
          phone: message.phone,
        },
      })
      .then(
        result => {},
        err => {}
      );
  }
}
