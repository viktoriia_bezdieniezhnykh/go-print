// PaymentTransaction
import { Column, DataType, IsUUID, Model, PrimaryKey, Table, Comment } from 'sequelize-typescript';

@Table
export class PaymentTransaction extends Model<PaymentTransaction> {
  @IsUUID(4)
  @PrimaryKey
  @Column({defaultValue: DataType.UUIDV4})
  id: string;

  @Column(DataType.ARRAY(DataType.STRING))
  ids: string[];

  @Column(DataType.JSON)
  result: any;

  @Comment('"payment" or "verification" or "task_verification" or "CREDIT2CARDTOKEN"')
  @Column
  type: string;

  @Column
  status: string;
}
