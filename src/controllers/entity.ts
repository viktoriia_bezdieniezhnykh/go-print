import { Router } from 'express';

import { getModel, getSeqlize } from '../db';
import { logger } from '../utils/logger'; // '../server';


// tslint:disable-next-line: restrict-plus-operands
const getModelName = (name) => name.charAt(0).toUpperCase() + name.slice(1);

export const EntityMiddleware = async (req, res, next) => {
  try {
    const type = req.params.entityType;
    const id = req.params.entityId;
    const includesRequest = req.query.includes;
    delete req.query.includes;
    if (type && id) {
      const seq = getSeqlize();

      const modelName = getModelName(type);
      const model: any = seq.modelManager.getModel(modelName);

      const includes = [];
      if (includesRequest) {
        const includeNames = includesRequest; //.split(',');
        for (const includeName of includeNames) {
          if (typeof includeName === 'string' || includeName.deep) {
            includes.push({
              model: getModel(includeName),
              include: [{ all: true, nested: true }],
            });
          } else {
            includes.push({
              model: getModel(includeName),
            });
          }
        }
      }

      const e = await model.findOne({
        where: { id },
        include: includes,
      });

      req.entity = e;
    } else {
      req.entity = null;
      res.status(404).send('Entity not found');
    }
    next();
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
};

export const EntityTypeMiddleware = (req, res, next) => {
  try {
    const type = req.params.entityType;
    if (type) {
      const model: any = getModel(type);
      req.model = model;
    } else {
      res.status(404).send('Entity type not found');
    }
    next();
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
};

const roleMIddleware = (req, res, next) => {
  const { role, id } = req.user;
  if (role !== 2) {
    const userId = id ? id : 0;
    const where = req.query.where && JSON.parse(req.query.where);
    req.query.where = JSON.stringify({...where, userId});
  }
  next();
};

const router: Router = Router();

router.get('/:entityType', roleMIddleware, async (req, res) => {
  try {
    const type = req.params.entityType;
    const includesRequest = req.query.includes;
    delete req.query.includes;

    const shallowInclude = req.query.shallowInclude ? true : false;
    delete req.query.shallowInclude;

    // Where
    if (typeof req.query.where !== 'undefined') {
      req.query.where = JSON.parse(req.query.where as any);
    }

    // Includes
    const includes = [];
    if (includesRequest) {
      // const includeNames = JSON.parse(includesRequest); // .split(',');
      const includeNames = includesRequest as any; // .split(',');
      for (const includeName of includeNames) {
        if (shallowInclude) {
          includes.push({
            model: getModel(includeName as any),
          });
        } else if (typeof includeName === 'string' || includeName.deep) {
          includes.push({
            model: getModel(includeName as any),
            include: [{ all: true }],
          });
        } else {
          includes.push({
            model: getModel(includeName as any),
          });
        }
      }
      req.query.include = includes;
    }

    // Order
    if (typeof req.query.order !== 'undefined') {
      req.query.order = JSON.parse(req.query.order as any);
    }

    const model: any = getModel(type);
    const entities = await model.findAndCountAll(req.query);

    res.json(entities);
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});

router.get('/:entityType/one', EntityTypeMiddleware, async (req, res) => {
  try {
    const type = req.params.entityType;
    const includesRequest = req.query.includes;
    delete req.query.includes;

    // Where
    if (typeof req.query.where !== 'undefined') {
      req.query.where = JSON.parse(req.query.where as any);
    }

    // Includes
    const includes = [];
    if (includesRequest) {
      const includeNames = JSON.parse(includesRequest as any); // .split(',');
      for (const includeName of includeNames) {
        includes.push({
          model: getModel(includeName),
        });
      }
      req.query.include = includes;
    }
    const entity = await req.model.findOne(req.query);

    res.json(entity);
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});

router.get('/:entityType/count', EntityTypeMiddleware, async (req, res) => {
  try {
    const type = req.params.entityType;
    const includesRequest = req.query.includes;
    delete req.query.includes;

    // Where
    if (typeof req.query.where !== 'undefined') {
      req.query.where = JSON.parse(req.query.where as any);
    }

    // Includes
    const includes = [];
    if (includesRequest) {
      const includeNames = JSON.parse(includesRequest as any); // .split(',');
      for (const includeName of includeNames) {
        includes.push({
          model: getModel(includeName),
        });
      }
      req.query.include = includes;
    }
    const total = await req.model.count(req.query);

    res.json(total);
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});

router.get('/:entityType/:entityId', EntityMiddleware, async (req, res) => {
  try {
    res.json(req.entity);
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});

router.patch(
  '/:entityType/:entityId/:operation',
  EntityMiddleware,
  async (req, res) => {
    try {
      await req.entity[req.params.operation](req.body);

      res.json(req.entity);
    } catch (error) {
      logger.error(error.message);
      res.status(500).send(error.message);
    }
  }
);

router.post('/:entityType', EntityTypeMiddleware, async (req, res) => {
  try {
    const entity = await req.model.create(req.body);

    res.json(entity);
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});

router.patch('/:entityType/:entityId', EntityMiddleware, async (req, res) => {
  try {
    const entity = await req.entity.update(req.body);
    res.json(entity);
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});

router.delete('/:entityType/:entityId', EntityMiddleware, async (req, res) => {
  try {
    await req.entity.destroy({ force: true });
    res.send('');
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});


export const EntityController: Router = router;
