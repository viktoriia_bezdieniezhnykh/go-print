import { Router } from 'express';
import * as jwt from 'jsonwebtoken';
import Axios from 'axios';
const crypto = require('crypto');
import * as appleSignin from 'apple-signin-auth';

import { User } from '../models/User';
import { safeAttributes } from '../data/safeAttributes';
import { CreditCard } from '../models/CreditCard';
import { EmailSender } from '../utils/email_sender';
import { authUser, createUser } from '../utils/auth';
import { logger } from '../utils/logger'; // '../server';

const router: Router = Router();

router.get('/', async (req, res) => {
  try {
    const user = await User.findByPk(req.user.id, {
      attributes: safeAttributes,
      include: [
        {
          model: CreditCard,
          order: [['id', 'DESC']],
          where: { active: true },
          required: false,
        },
      ],
    });
    if (!user) {
      return res.status(404).send('User not found');
    }
    res.json(user);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/creditcards', async (req, res) => {
  try {
    const cards = await CreditCard.findAll({ where: { userId: req.user.id, active: true } });
    res.json(cards);
  } catch ({ message }) {
    res.status(500).send(message);
  }
});

router.delete('/card/:cardId', async (req, res) => {
  try {
    const card = await CreditCard.findByPk(req.params.cardId);
    await card.destroy({ force: true });
    res.send('');
  } catch ({ message }) {
    res.status(500).send(message);
  }
});

router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!email || !password) {
      return res.status(400).send('Please send email and password');
    }

    res.json(await authUser({ email, password }));
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/register', async (req, res) => {
  try {
    logger.info('register', { data: req.body });
    const { email, password } = req.body;
    if (!email || !password) {
      return res.status(400).send('Please send email and password');
    }

    res.send(createUser(req.body));
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/fb', async (req, res) => {
  try {
    const { email, name, image } = req.body;

    if (!email) {
      return res.status(500).json({msg: 'Please provide email'})
    }

    const payload = req.body;
    const access_token = '784605558694545|3XtJy-VQAQinl4rBTAVIxU1dg38';
    const checkFbToken = await Axios.get(
      'https://graph.facebook.com/debug_token',
      {
        params: { input_token: req.body.token, access_token: access_token },
      }
    );

    if (
      checkFbToken &&
      checkFbToken.data &&
      checkFbToken.data.data &&
      checkFbToken.data.data.is_valid
    ) {
      const exist = await User.findOne({ where: { email } });
      if (exist) {
        exist.name = name;
        if (image) {
          exist.image = image;
        }
        exist.lastSeen = new Date();
        await exist.save();

        const u = exist.toJSON();

        const { id, lastSeen } = exist;
        const expire = Math.floor(Date.now() / 1000) + 60 * 43200;
        const token = jwt.sign({ id, exp: expire }, process.env.SALT);
        return res.send({
          token,
          expire,
          user: u,
          new: false
        });
      }

      const user = await User.create(payload);

      const expire = Math.floor(Date.now() / 1000) + 60 * 43200;
      const token = jwt.sign({ id: user.id, exp: expire }, process.env.SALT);
      const u = user.toJSON();

      res.send({ token, expire, user: u, new: true });
    } else {
      res.status(401).send('FB token is not valid');
    }
  } catch ({ message }) {
    logger.error(message, { lable: 'Facebook auth error' });
    res.status(500).send(message);
  }
});

router.post('/apple', async (req, res) => {
  try {
    const { fullName, identityToken } = req.body;

    const clientId = process.env.APP_ID;
    // verify token (will throw error if failure)
    const { sub, email } = await
      appleSignin.verifyIdToken(identityToken, {
        audience: clientId,
        ignoreExpiration: true, // ignore token expiry (never expires)
      });

    if (
      sub === req.body.user
    ) {
      const exist = await User.findOne({ where: { email } });
      if (exist) {
        exist.lastSeen = new Date();
        await exist.save();

        const u = exist.toJSON();

        const { id, lastSeen } = exist;
        const expire = Math.floor(Date.now() / 1000) + 60 * 43200;
        const token = jwt.sign({ id, exp: expire }, process.env.SALT);
        return res.send({
          token,
          expire,
          user: u,
          new: false
        });
      }

      const user = await User.create({
        email,
        name: Object.values(fullName).join(' ')
      });

      const expire = Math.floor(Date.now() / 1000) + 60 * 43200;
      const token = jwt.sign({ id: user.id, exp: expire }, process.env.SALT);
      const u = user.toJSON();

      res.send({ token, expire, user: u, new: true });
    } else {
      res.status(401).send('Apple token is not valid');
    }
  } catch ({ message }) {
    logger.error(message, { lable: 'Apple auth error' });
    res.status(500).send(message);
  }
});

router.patch('/setDefaultCard', async (req, res) => {
  try {
    let { isDefault, cardId } = req.body;
    if (isDefault) {
      const updateOther = await CreditCard.update(
        { default: false },
        { where: { userId: req.user.id } }
      );
    }
    const updateDefault = await CreditCard.update(
      { default: isDefault },
      { where: { id: cardId } }
    );
    const cards = await CreditCard.findAll({
      where: { userId: req.user.id, active: true },
      order: [['id', 'DESC']],
    });
    res.send({ cards });
  } catch (err) {
    logger.error(err.message, { lable: 'setDefaultCard' });
    res.status(500).send(err.message);
  }
});

router.get('/checkPassToken/:token', async (req, res) => {
  try {
    const user = await User.findOne({ where: { resetToken: req.params.token } });
    if (user) {
      res.send({ result: 'confirmed' });
    } else {
      res.send({ result: 'no_token' })
    }
  } catch (err) {
    logger.error(err);
    res.status(500).send(err.message);
  }
});

router.patch('/setNewPass/:token', async (req, res) => {
  try {
    const user = await User.findOne({ where: { resetToken: req.params.token } });
    if (user) {
      const updated = await user.update({
        hash: req.body.newPass, resetToken: null, resetDate: null
      });
      res.send({ result: 'confirmed' });
    } else {
      res.send({ result: 'no_token' })
    }
  } catch (err) {
    logger.error(err);
    res.status(500).send(err.message);
  }
});

router.patch('/restorePass', async (req, res) => {
  try {
    const user = await User.findOne({ where: { email: req.body.email } });
    if (user) {
      const password = crypto.randomBytes(8).toString('hex');
      const updated = await user.update({
        resetToken: password,
        resetDate: new Date()
      });
      new EmailSender().sendEmail({
        to: user.email,
        subject: `[GO Print] Відновлення пароля`,
        template: 'restoreUserPassword',
        variables: {
          login: user.email,
          link: `${process.env.FRONTEND}?t=${password}`,
        },
      })
        .then(res => logger.info(`EmailSender`, { data: res }))
        .catch((err) => logger.error(err));
      res.json('sending');
    } else {
      res.status(500).json('no_user')
    }
  } catch (err) {
    logger.error(err);
    res.status(500).send(err.message);
  }
});

export const UserController: Router = router;
