import * as request from 'supertest'
import { app } from '@goprint/app';
import { getSeqlize, getModel } from '@goprint/db';
import * as faker from 'faker';

import { authUser, createUser } from "@goprint/utils/auth";

describe("test user controller", () => {
  // Set the db object to a variable which can be accessed throughout the whole test file
  let thisDb: any = getSeqlize()
  let testUser;

  const randomString = faker.random.alphaNumeric(10);
  const email = `user-${randomString}@email.com`;
  const password = `password`;

  // Before any tests run, clear the DB and run migrations with Sequelize sync()
  beforeAll(async () => {
    await thisDb.sync({ force: true })
    let { user } = await createUser({ email, password })
    testUser = user;
  })

  it("should succeed when accessing an authed route with a valid JWT", async () => {
    const { token } = await authUser({
      email,
      password,
    })

    // App is used with supertest to simulate server request
    const response = await request(app)
      .get("/user")
      .expect(200)
      .set("authorization", `bearer ${token}`)

    expect(response.body).toMatchObject({
      "balance": "0.00",
      "cards": [],
      "email": email,
      "id": 1,
      "image": null,
      "name": null,
      "phoneConfirmed": null,
    })
  })

  it("should succeed when cards array returned", async () => {
    const { token } = await authUser({
      email,
      password,
    })

    // App is used with supertest to simulate server request
    const response = await request(app)
      .get("/user/creditcards")
      .expect(200)
      .set("authorization", `bearer ${token}`)

    expect(Array.isArray(response.body)).toBe(true);
  })

  it("should succeed when default card setted", async () => {
    const card = await getModel('CreditCard').create({
      cardToken: '1',
      cardName: '1',
      userId: testUser.id
    });

    const { token } = await authUser({
      email,
      password,
    })

    // App is used with supertest to simulate server request
    const response = await request(app)
      .patch(`/user/setDefaultCard`)
      .send({ isDefault: true, cardId: card.id })
      .expect(200)
      .set("authorization", `bearer ${token}`)
    
    expect(Array.isArray(response.body.cards)).toBe(true);
  })

  it("should succeed when card deleted", async () => {
    const card = await getModel('CreditCard').create({
      cardToken: '1',
      cardName: '1',
      userId: testUser.id
    });

    const { token } = await authUser({
      email,
      password,
    })

    // App is used with supertest to simulate server request
    await request(app)
      .delete(`/user/card/${card.id}`)
      .expect(200)
      .set("authorization", `bearer ${token}`)
  })

  it("should succeed on valid resettoken change", async () => {
    const { token } = await authUser({
      email,
      password,
    })

    const resetToken = await testUser.sendResetLink();

    // App is used with supertest to simulate server request
    await request(app)
      .get(`/user/checkPassToken/${resetToken}`)
      .expect(200)
      .set("authorization", `bearer ${token}`)
  })

  it("should succeed on successful password change", async () => {
    const { token } = await authUser({
      email,
      password,
    })

    const resetToken = await testUser.sendResetLink();

    // App is used with supertest to simulate server request
    const response = await request(app)
      .patch(`/user/setNewPass/${resetToken}`)
      .send({newPass: password})
      .expect(200)
      .set("authorization", `bearer ${token}`)

    expect(response.body).toMatchObject({ result: 'confirmed' });
  })

  it("should succeed on password reset", async () => {
    const { token } = await authUser({
      email,
      password,
    })

    // App is used with supertest to simulate server request
    const response = await request(app)
      .patch(`/user/restorePass`)
      .send({email})
      .expect(200)
      .set("authorization", `bearer ${token}`)

    expect(response.body).toBe('sending');
  })

  it("should fail when accessing an authed route with an invalid JWT", async () => {
    const invalidJwt = "fakeToken"

    const response = await request(app)
      .get("/user")
      .expect(401)
      .set("authorization", `bearer ${invalidJwt}`)

    expect(response.body).toMatchObject({
      message: "jwt malformed",
    })
  })

  // After all tersts have finished, close the DB connection
  afterAll(async (done) => {
    await thisDb.close();
    done();
  })
})
