import { Router } from 'express';
import { parsePhoneNumberFromString } from 'libphonenumber-js';

import { Code } from '../models/Code';
import { User } from '../models/User';
import { updateOrCreate } from '../utils/update-or-create';
import { phoneNormalizer } from '../utils/normalizers';

import { sendSMS } from '../utils/turbosmsClient';
import { logger } from '../utils/logger'; // '../server';

const router: Router = Router();

router.post('/check', phoneNormalizer, async (req, res) => {
  try {
    if (!req.body.code) {
      return res.status(400).json('Wrong code!');
    }

    if (!req.body.phone) {
      return res.status(400).json('Wrong phone!');
    }

    const code = await Code.findOne({
      where: {
        phone: req.body.phone,
        code: req.body.code,
      },
    });

    if (code.used) {
      return res.status(400).json('Code used');
    }

    if (code) {
      code.used = true;
      await code.save();

      let user = await User.findOne({
        where: { id: req.user.id },
      });

      user.phoneConfirmed = true;
      user.balance = Number(user.balance) + 15;
      user.phone = req.body.phone;
      await user.save();
      
      res.send({ user });
    } else {
      res.status(400).send('Wrong code!');
    }
  } catch (error) {
    logger.error(error.message);
    res.status(500).send(error.message);
  }
});

router.post('/', phoneNormalizer, async (req, res) => {
  try {
    if (!req.body.phone) {
      return res.status(400).json('Phone not valid!');
    }
    if (req.body.phone.lenght < 9) {
      throw new Error('Phone too short');
    }

    const phoneCode = await Code.findOne({
      where: {
        phone: req.body.phone,
      },
    });

    if (phoneCode && phoneCode.used) {
      return res.status(400).json('Code already used!');
    }

    let code: any = '000000';
    if (process.env.NODE_ENV === 'production' && process.env.SMS_BLOCKED !== 'true') {
      code = Math.floor(Math.random() * 900000 + 100000);
    }

    await updateOrCreate(
      Code,
      {
        phone: req.body.phone,
      },
      {
        phone: req.body.phone,
        code,
      }
    );
    logger.info('Code', code);
    
    // send sms
    if (process.env.NODE_ENV === 'production') {
      await sendSMS({sender: 'GoPrint', phone: req.body.phone, text: code});
    }    

    return res.json('ok');
  } catch (error) {
    logger.error(error.message);    
    res.status(500).send(error.message);
  }
});

export const CodesController: Router = router;
