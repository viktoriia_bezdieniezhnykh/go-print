import { Router } from 'express';
import * as jwt from 'jsonwebtoken';
import Axios from 'axios';
const crypto = require('crypto');

import { User } from '../../models/User';
import { safeAttributes } from '../../data/safeAttributes';
import { CreditCard } from '../../models/CreditCard';
import { logger } from '../../utils/logger'; // '../../server';

const router: Router = Router();

router.get('/', async (req, res) => {
  try {
    const user = await User.findByPk(req.user.id, {
      attributes: safeAttributes,
      include: [{
        model: CreditCard,
        order: [['id', 'DESC']],
        where: {active: true},
        required: false
      }]
    });
    if (!user) {
      return res.status(404).send('User not found');
    }

    res.json(user);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({
      where: { email },
      attributes: ['hash', 'roleId', ...safeAttributes],
      include: [CreditCard]
    });

    if (!user) {
      return res.status(404).send('User not found');
    }

    const hash = crypto
      .createHash('md5')
      .update(`${password}|${process.env.SALT}|pass`)
      .digest('hex');

    if (user.hash !== hash) {
      return res.status(401).send('Wrong password');
    }

    // if (user.roleId !== 2 ) {
    //   return res.status(403).send('User not admin');
    // }

    user.lastSeen = new Date();
    await user.save();

    const u = user.toJSON();
    delete u['hash'];
    delete u['createdAt'];
    delete u['updatedAt'];

    const expire = Math.floor(Date.now() / 1000) + 60 * 540;
    const token = jwt.sign({ id: user.id, exp: expire, role: user.roleId }, process.env.SALT);
    res.json({ token, expire, user: u });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/register', async (req, res) => {
  try {
    const { email, password } = req.body;
    const payload = req.body;

    const exist = await User.findOne({ where: { email } });
    if (exist) {
      return res.status(400).send('User exist');
    }

    const hash = crypto
      .createHash('md5')
      .update(`${password}|${process.env.SALT}|pass`)
      .digest('hex');

    payload.hash = hash;

    const user = await User.create(payload);
    const { id, name, lastSeen } = user;
    const expire = Math.floor(Date.now() / 1000) + 60 * 540;
    const token = jwt.sign({ id: user.id, exp: expire }, process.env.SALT);
    res.send({ token, expire, user: { id, name, lastSeen, email } });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/fb', async (req, res) => {
  try {
    const { email, name, image } = req.body;
    const payload = req.body;
    const access_token = '784605558694545|3XtJy-VQAQinl4rBTAVIxU1dg38';
    const checkFbToken = await Axios.get(
      'https://graph.facebook.com/debug_token', {
        params: {input_token: req.body.token, access_token: access_token}
      }
    );

    if (checkFbToken && checkFbToken.data && checkFbToken.data.data &&
      checkFbToken.data.data.is_valid) {
      const exist = await User.findOne({ where: { email } });
      if (exist) {
        exist.name = name;
        if (image) {
          exist.image = image;
        }
        exist.lastSeen = new Date();
        await exist.save();

        const {id, lastSeen} = exist;
        const expire = Math.floor(Date.now() / 1000) + 60 * 540;
        const token = jwt.sign({ id, exp: expire }, process.env.SALT);
        return res.send({ token, expire, user: {id, name, lastSeen, email, image} });
      }

      // const password = crypto.randomBytes(4).toString('hex');
      const password = '123';
      const hash = crypto
        .createHash('md5')
        .update(`${password}|${process.env.SALT}|pass`)
        .digest('hex');

      payload.hash = hash;

      const user = await User.create(payload);
      const {id, lastSeen} = user;
      const expire = Math.floor(Date.now() / 1000) + 60 * 540;
      const token = jwt.sign({ id: user.id, exp: expire }, process.env.SALT);
      res.send({ token, expire, user: {id, name, lastSeen, email, image} });
    } else {
      res.status(401).send('FB token is not valid');
    }
  } catch ({message}) {
    res.status(500).send(message);
  }
});

router.patch('/setDefaultCard', async (req, res) => {
  try {
    let {isDefault, cardId} = req.body;
    if (isDefault) {
      const updateOther = await CreditCard.update({default: false}, {where: {userId: req.user.id}});
    }
    const updateDefault = await CreditCard.update({default: isDefault}, {where: {id: cardId}});
    const cards = await CreditCard.findAll({where: {userId: req.user.id}, order: [['id', 'DESC']]});
    res.send({ cards });
  } catch(err) {
    logger.error(err);
    res.status(500).send(err.message);
  }
});

export const AdminAuthController: Router = router;
