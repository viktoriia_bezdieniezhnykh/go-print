import { Router } from 'express';
import { User } from '../../models/User';
import { LocalFile } from '../../models/LocalFile';
import { existsSync, createReadStream } from 'fs';

import * as jwt from 'jsonwebtoken';

const router: Router = Router();
router.post('/', async (req, res) => {
  try {
    const userCreate = await User.findOrCreate({
      where: { email: req.body.email },
      defaults: req.body,
    });
    
    if (userCreate.length > 1 && !userCreate[1]) {
      res.status(400).json('email_exists');
    } else {
      res.json(userCreate[0]);
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).json(error.message);
  }
});

router.get('/file/:id', async (req, res) => {
  try {
    const {token} = req.query;
    jwt.verify(token, process.env.SALT);

    const file = await LocalFile.findByPk(req.params.id);
    if (existsSync(file.path)) {
      createReadStream(file.path).pipe(res);
    } else {
      res.status(404);
      res.send('No such file');
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).json(error.message);
  }
});

export const AdminUserController: Router = router;
