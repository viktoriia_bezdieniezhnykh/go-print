import { Router } from 'express';
import { Task } from '../../models/Task';
import { Sequelize, Op } from 'sequelize';
import { Printer } from '../../models/Printer';
import { logger } from '../../utils/logger'; // '../../server';

const router: Router = Router();

router.get('/', async (req, res) => {
  try {
    let tasksAmount = [];
    switch (req.user.role) {
      case 4:
        tasksAmount = await Task.findAll({
          where: {
            '$printer.userId$': req.user.id,
          },
          attributes: [
            'Task.status',
            [Sequelize.fn('COUNT', Sequelize.col('Task.id')), 'total'],
          ],
          group: ['Task.status', 'printer.id'],
          include: [
            {
              model: Printer,
            },
          ],
          raw: true,
        });
        break;

      default:
        tasksAmount = await Task.findAll({
          attributes: [
            'Task.status',
            [Sequelize.fn('COUNT', Sequelize.col('Task.id')), 'total'],
          ],
          group: ['Task.status'],
          raw: true,
        });
        break;
    }

    const result = {};
    for (const stat of tasksAmount) {
      result[stat.status] = stat['total'];
    }
    res.json(result);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/printer/:printerId', async (req, res) => {
  try {
    const { printerId } = req.params;
    const tasksData = await Task.findAll({
      where: { printerId },
      attributes: [
        'status',
        [Sequelize.fn('COUNT', Sequelize.col('id')), 'total'],
        [Sequelize.fn('SUM', Sequelize.col('cost')), 'total'],
      ],
      group: ['status'],
      raw: true,
    });

    res.json(tasksData);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/grouped', async (req, res) => {
  try {
    let where: any = { payed: true, test: { [Op.ne]: true } };

    if (req.user.role !== 2) {
      where = { ...where, '$printer.userId$': req.user.id };
    }

    let literalCondition = '';
    const { from, to } = req.query;
    if (from && to) {
      where = { ...where, createdAt: { [Op.between]: [from, to] } };
      literalCondition = ` AND subtask."createdAt" BETWEEN '${from}' AND '${to}' `;
    }

    const tasksData = await Task.findAll({
      where,
      attributes: [
        [Sequelize.fn('COUNT', Sequelize.col('Task.id')), 'total'],
        [Sequelize.fn('SUM', Sequelize.col('Task.cost')), 'totalCost'],
        [
          Sequelize.literal(
            `(SELECT SUM(subtask.cost)
              FROM "Tasks" as subtask
              WHERE subtask."printConfig"->>'color'='color'
              AND subtask.payed=true
              AND subtask.test!=true
              ${literalCondition}
              AND "subtask"."printerId" = "printer"."id")`
          ),
          'color',
        ],
        [
          Sequelize.literal(
            `(SELECT SUM(subtask.cost)
              FROM "Tasks" as subtask
              WHERE subtask."printConfig"->>'color'='bw'
              AND subtask.payed=true
              AND subtask.test!=true
              ${literalCondition}
              AND "subtask"."printerId" = "printer"."id")`
          ),
          'bw',
        ],
      ],
      group: ['printer.id'],
      include: ['printer'],
    });

    res.json(tasksData);
  } catch (err) {
    const { message } = err;
    logger.error(err)
    res.status(500).send(message);
  }
});

export const AdminStatisticsController: Router = router;
