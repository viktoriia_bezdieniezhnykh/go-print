import { Router } from 'express';
import * as ipp from 'ipp';

import { Printer } from '../models/Printer';
import {
  getPrinterUrls,
  watchPrintJob,
  printOn,
  getPrinterAttributes,
  sendDataToPrinter,
  attributesFromTask,
  getPrinterJobs
} from '../utils/cups-util';
import Axios from 'axios';
import { readFileSync, createReadStream, createWriteStream } from 'fs';
import { join } from 'path';
import FormData = require('form-data');
import { LocalFile } from '../models/LocalFile';
import { convertFile } from '../utils/files';
import { logger } from '../utils/logger'; // '../server';

const publicDir = join(__dirname, '../..', 'public/temp');

const router: Router = Router();

router.get('/', async (req, res) => {
  try {
    const printers: string[] = await getPrinterUrls();
    const printer = ipp.Printer(`ipp://${process.env.CUPS_SERVER}/printers/Printer1`);

    const data = Buffer.from('Text', 'utf8');
    // const job = await printOn(printer, data);

    const job = await sendDataToPrinter(printer, data, {
      'number-up': 4,
      sides: 'two-sided-short-edge',
      'print-color-mode': 'monochrome',
      'orientation-requested': 'landscape',
    });

    const taskEvents = req.app.get('taskEvents');
    watchPrintJob(printer, job, 1)
      .then(
        // push success event
        res => {
          logger.info('taskEvents watched', { data: res });
          taskEvents.taskComplete(1);
        }
      )
      .catch(
        // push error event
        (err) => {
          logger.error(err);
          taskEvents.taskCanceled(1);
        }
      );

    res.json('ok');
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/info', async (req, res) => {
  try {
    const printers: string[] = await getPrinterUrls();
    
    const printer = ipp.Printer(`ipp://${process.env.CUPS_SERVER}/printers/Printer4`);
    const attr = await getPrinterAttributes(printer, []);

    res.json('ok');
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/get/printers', async (req, res) => {
  try {
    const printers: string[] = await getPrinterUrls();
    
    printers.forEach(async printer => {
      const name = String(printer)
        .split('/')
        .pop();
      await Printer.findOrCreate({
        where: { uri: printer },
        defaults: {
          name,
          uri: printer,
          status: 'idle',
        },
      });
    });

    const printersDb = await Printer.findAll();

    res.json('ok');
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/', async (req, res) => {
  try {
    res.json('ok');
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/convert', async (req, res) => {
  try {
    const localFile = await LocalFile.findOne();
    const newFile = await convertFile(localFile);
    res.json(newFile);
  } catch ({ message, response }) {
    res.status(500).send(message);
  }
});

router.get('/getJobs', async (req, res) => {
  try {
    const printer = ipp.Printer(`ipp://${process.env.CUPS_SERVER}/printers/Printer1`);

    const status = await getPrinterJobs(printer);
    res.json(status);
  } catch ({ message }) {
    res.status(500).send(message);
  }
});

export const PrintController: Router = router;
