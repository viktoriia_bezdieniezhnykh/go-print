import { Router } from 'express';
import { Op } from 'sequelize';
import * as ipp from 'ipp';

import { Printer } from '../models/Printer';
import { upload } from '../utils/multer';
import { Task } from '../models/Task';
import { EmailSender } from '../utils/email_sender';
import { getJobAttributes, getPrinterJobs } from '../utils/cups-util';
import { isArray } from 'underscore';
import { logger } from '../utils/logger'; // '../server';

const router: Router = Router();

router.get('/', async (req, res) => {
  try {
    const printers = await Printer.findAll({
      attributes: [
        'id',
        'name',
        'latlng',
        'description',
        'region',
        'state',
        'status',
        'location',
        'address'
      ],
    });
    res.json(printers);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/printFinished/:id', async (req, res) => {
  try {
    const updateStatus = await Task.update(
      { status: 'complete' },
      { where: { id: req.params.id } }
    );
    const taskEvents = res.app.get('taskEvents');
    taskEvents.taskComplete(req.params.id);
    res.json({ result: 'ok' });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get('/state', async (req, res) => {
  try {
    const { terminal, state } = req.query;

    const printer = await Printer.findOne({
      where: { uri: { $like: `%/${terminal}` }, status: { [Op.ne]: 'stopped' } },
    });

    if (!printer) {
      return res.send('not-active-printer');
    }

    await printer.changeStatus(state);

    return res.send('ok');
  } catch ({ message }) {
    res.status(500).send(message);
  }
});

router.get('/jobs', async (req, res) => {
  try {
    const { printerId } = req.query;

    const printer = await Printer.findByPk(printerId);

    if (!printer) {
      return res.send('not-active-printer');
    }

    const printerInst = ipp.Printer(printer.uri);
    const pJobs = await getPrinterJobs(printerInst);

    const jobsData = [];
    logger.info(pJobs);

    if (isArray(pJobs["job-attributes-tag"])) {
      for (const job of pJobs["job-attributes-tag"]) {
        const jData = await getJobAttributes(printerInst, job["job-id"]);
        jobsData.push(jData);
      }
    } else {
      const jData = await getJobAttributes(printerInst, pJobs["job-attributes-tag"]["job-id"]);
      jobsData.push(jData);
    }



    return res.json(jobsData);
  } catch ({ message }) {
    res.status(500).send(message);
  }
});

export const PrinterController: Router = router;
