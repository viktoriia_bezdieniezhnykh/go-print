import { Router } from 'express';
import { Op } from 'sequelize';

import { CreditCard } from '../models/CreditCard';
import { PaymentTransaction } from '../models/PaymentTransaction';
import { Task } from '../models/Task';
import { logger } from '../utils/logger'; // '../server';
import { generateForm } from '../utils/payments';
import { updateOrCreate } from '../utils/update-or-create';

const router: Router = Router();

router.post('/process', async (req, res) => {
  try {
    const orider_id = req.body.action === 'RECURRING_SALE' ? req.body.order_id : req.body.order;
    const transaction = await PaymentTransaction.findByPk(orider_id);
    logger.info('transaction', transaction.toJSON());
    if (transaction) {
      switch (transaction.type) {
        case 'verification':
          const cardsEvents = res.app.get('cardsEvents');
          const ccardData = {
            cardToken: req.body.card_token,
            rcToken: req.body.rc_token,
            rcId: req.body.rc_id,
            cardMask: req.body.card,
            userId: transaction.ids[0],
          };
          if (ccardData) {
            const { item, created } = await updateOrCreate(
              CreditCard,
              { cardToken: req.body.card_token },
              ccardData
            );
            cardsEvents.cardApproved(ccardData.userId, {
              approved: true,
              cardId: item.id,
            });
          }

          return res.send('ok');
          break;
        case 'recurring':
          return res.send('ok');
          break;
        default:
          const paymentEvents = res.app.get('paymentEvents');
          const tasks = await Task.findAll({
            where: { id: { [Op.in]: transaction.ids } },
          });
          for (const task of tasks) {
            await task.pay();
          }

          // save card
          let cards = [];
          if (req.body.card_token) {
            const cardData = {
              cardToken: req.body.card_token,
              rcToken: req.body.rc_token,
              rcId: req.body.rc_id,
              cardMask: req.body.card,
              userId: tasks[0].userId,
            };
            await updateOrCreate(
              CreditCard,
              { cardToken: req.body.card_token },
              cardData
            );

            cards = await CreditCard.findAll({
              where: { userId: tasks[0].userId },
            });
          }

          transaction.ids.forEach(id =>
            paymentEvents.paymentSucceed(id, {
              success: true,
              cards,
            })
          );

          return res.send('ok');
      }
    }
  } catch (error) {
    logger.error(error);
    res.send('ok');
  }
});

router.post('/:type', async (req, res) => {
  try {
    // create transaction
    let ids = [];
    if (req.params.type === 'verification') {
      ids = [req.body.userId];
    } else if (req.params.type === 'payment') {
      ids = [req.body.taskId];
      // load price
      const task = await Task.findByPk(req.body.taskId);
      if (!task) {
        throw new Error('No such task');
      }
      req.body.amount = task.cost;
    }

    const data = {
      type: req.params.type,
      ids,
    };
    const transaction = await PaymentTransaction.create(data);
    logger.info('transaction id', transaction.id);
    const from = generateForm({
      type: req.params.type,
      orderId: transaction.id,
      detailsData: req.body,
    });
    res.json(from);
  } catch (error) {
    logger.error(error);
    res.status(500).send(error.message);
  }
});

export const PaymentController: Router = router;
