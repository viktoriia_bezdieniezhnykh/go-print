import { Router } from 'express';
import * as ipp from 'ipp';
import { FindOptions, Op } from 'sequelize';

import { Printer } from '../models/Printer';
import {
  getPrinterUrls,
  printOn,
  watchPrintJob,
  getPrinterAttributes,
} from '../utils/cups-util';
import { LocalFile } from '../models/LocalFile';
import { Task } from '../models/Task';
import {
  countPages,
  convertFile,
  combineImagesToPdf,
  extractFileData,
  fitPdfToPage,
  convertFileGoten, convertToJpeg, resavePdf
} from '../utils/files';
import { upload } from '../utils/multer';
import { prices } from '../data/price';
import { User } from '../models/User';
import Axios from 'axios';
import { PaymentTransaction } from '../models/PaymentTransaction';
import { CreditCard } from '../models/CreditCard';
import { createHash } from 'crypto';
import FormData = require('form-data');
import { logger } from '../utils/logger'; // '../server';

const router: Router = Router();

router.get('/', async (req, res) => {
  try {
    let include = req.query.includePrinter
      ? [{ model: Printer, required: false }]
      : [];
    delete req.query.includePrinter;
    let limit = req.query.limit ? Number(req.query.limit) : 0;
    delete req.query.limit;

    const findOptions: FindOptions = {
      where: {
        userId: req.user.id,
        archived: false,
        status: { [Op.ne]: 'expired' },
        ...req.query,
      },
      order: [['id', 'DESC']],
      include: ['files'],
    };
    if (include.length > 0) {
      findOptions['include'] = include;
    }
    if (limit > 0) {
      findOptions['limit'] = limit;
    }
    const tasks = await Task.findAll(findOptions);
    res.json(tasks);
  } catch (error) {
    logger.error(`Error on get task: ${error.message}`)
    res.status(500).send(error.message);
  }
});

const convertibleMimes = [
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/msword',
  'application/vnd.ms-excel',
  'application/vnd.ms-powerpoint',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'application/vnd.oasis.opendocument.formula',
  'application/vnd.oasis.opendocument.presentation',
  'application/vnd.oasis.opendocument.spreadsheet',
  'application/vnd.oasis.opendocument.text',
  'application/rtf',
  'text/rtf',
];

const fitableMimes = [
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'application/vnd.oasis.opendocument.presentation',
];

// .doc, .docx, .jpg, .png, .pdf, .txt, .xls, .xlsx, .ppt, .pptx, .png, .odt, .odp, .ods, .rtf.
const validMimes = [
  'image/jpeg',
  'image/heic', // ridiculous apple image format
  'image/jpg',
  'image/png',
  'text/plain',
  'application/pdf',
  ...convertibleMimes,
];

const imageMimes = ['image/jpeg', 'image/jpg', 'image/png'];

router.post('/', upload.array('file'), async (req, res) => {
  try {
    const printConfig = JSON.parse(req.body.printConfig);

    const toCombine = [];

    for (let index = 0; index < req.files.length; index++) {
      try {
        const element = req.files[index];

        // logger.info('Get data about files');
        req.files[index] = await extractFileData(req.files[index]);

        if (req.files[index].mimetype === 'application/pdf') {
          req.files[index] = await resavePdf(req.files[index]);
          // logger.info('Pdf was resaved');          
        }
        
        if (convertibleMimes.includes(req.files[index].mimetype)) {
          // logger.info('Convert file', req.files[index]);
          const origMime = req.files[index].mimetype;
          req.files[index] = await convertFileGoten(req.files[index]);
          if (fitableMimes.includes(origMime)) {
            logger.info('Try fit to page after convert to pdf');
            req.files[index] = await fitPdfToPage(req.files[index]);
          }
        }       

        if (req.files[index].mimetype === 'image/png') {
          req.files[index] = await convertToJpeg(req.files[index]);
        }

        // if (imageMimes.includes(req.files[index].mimetype)) {
        //   toCombine.push(req.files[index]);
        //   // delete req.files[index];
        // }

        if (!validMimes.includes(req.files[index].mimetype)) {
          req.files[index].errored = true;
        }
        req.files[index].userId = req.user.id;
      } catch ({ message }) {
        req.files[index].errored = true;
        logger.error(`File convertation error: ${message}`, );
      }
    }

    if (toCombine.length) {
      try {
        const newFile = await combineImagesToPdf(toCombine);
        req.files = req.files.filter(
          (i) => toCombine.findIndex((j) => j.path === i.path) === -1
        );
        req.files.push(newFile);
      } catch (error) {
        logger.error(error.message);
      }
    }

    // logger.info('after', req.files);
    // !TODO Record only valid files to DB or all?
    const files = await LocalFile.bulkCreate(req.files, {
      returning: true,
    });

    const validFiles = files.filter((f) => !f.errored);

    if (validFiles.length === 0) {
      return res.status(415).send('No valid documents in request.');
    }

    const pagesQty = await countPages(validFiles);
    // const pagesQty = validFiles.reduce((total, file) => total + file.pages, 0);

    // !TODO Check for NaN
    const cost = pagesQty * prices[printConfig.color][printConfig.print];

    const task = await Task.create({
      printConfig,
      cost,
      pagesQty,
      userId: req.user.id,
      source: req.body.source,
    });
    await task.$add('files', files);

    logger.info('Task ready', task.toJSON());
    task.files = files;
    return res.json(task);
  } catch (error) {
    logger.error(`On task creation error - ${error.message}`);
    res.status(500).send(error.message);
  }
});

router.patch('/:id/add/printer', async (req, res) => {
  try {
    logger.info('Add printer to task', req.params.id, req.body);
    const task = await Task.findByPk(req.params.id);
    const { printerId } = req.body;
    task.printerId = printerId;
    await task.save();
    return res.json(task);
  } catch (error) {
    logger.error(`On add printer to task error: ${error.message}`);
    res.status(500).send(error.message);
  }
});

router.patch('/:id/print', async (req, res) => {
  try {
    logger.info(`Task ${req.params.id} start printing`, req.body);

    const task = await Task.findByPk(req.params.id);
    const printer: any = await task.$get('printer');

    if (!['payed'].includes(task.status) && req.user.role !== 2) {
      throw new Error(`Cant print task with "${task.status}" status`);
    }

    const printerInst = ipp.Printer(printer.uri);

    // check printer state
    const status = await getPrinterAttributes(printerInst);
    if (status['printer-attributes-tag']['printer-state'] !== 'idle') {
      if (
        status['printer-attributes-tag']['printer-state-message'] ===
        'The printer is not responding.'
      ) {
        printer.state = 'errored';
        await printer.save();
        throw new Error('The printer is not responding.');
      }
    }

    const { jobs, reason } = await task.print();
    if (!jobs.length) {
      switch (reason) {
        case 'jobExist':
          return res.status(400).json('Files already printing');
          break;
        case 'errorOnJobCreation':
          return res.status(400).json('Errors on job creation');
          break;
        case 'filesNotValid':
          return res.status(400).json('No valid files');
          break;
        default:
          return res.status(400).json('No valid jobs');
          break;
      }
    }

    const watchers = [];
    for (const job of jobs) {
      watchers.push(watchPrintJob(printerInst, job));
    }

    const taskEvents = req.app.get('taskEvents');
    Promise.all(watchers)
      .then( async (res) => {
        logger.info('watcher response' + JSON.stringify(res));
        if (res.length) {
          task.status = 'complete';
          taskEvents.taskComplete(task.id);
        } else {
          task.status = 'errored';
          taskEvents.taskCompleteWithErrors(task.id);
        }
        await task.save();
      })
      .catch(async ({ message }) => {
        logger.error(`'watcher error' - ${message}`);

        switch (message) {
          case 'Job is canceled - too many tries':
          case 'Job is canceled - timeout':
            taskEvents.taskCanceled(task.id);
            task.status = 'canceled';
            break;
        
          default:
            taskEvents.taskCompleteWithErrors(task.id);
            task.status = 'errored';
            break;
        }
        
        await task.save();
      });

    return res.json(task);
  } catch (error) {
    logger.error(`On job printing error: ${error.message}`);
    res.status(500).json(error.message);
  }
});

router.patch('/:id/pay', async (req, res) => {
  try {
    logger.error('Task payment endpoint', req.params.id, req.body);
    const task = await Task.findByPk(req.params.id);
    const user = await User.findByPk(req.user.id);

    let amount = task.cost;
    if (user.balance) {
      if (user.balance >= amount) {
        user.balance = user.balance - amount;
        amount = 0;
        await user.save();
      }
    }

    if (amount === 0) {
      await task.pay('balance');
      return res.json(task);
    }

    const cards: any = await user.$get('cards');

    let card;
    if (cards) {
      card = cards[0];
      if (cards.length > 1) {
        const defaultCard = cards.filter((c) => c.default).pop();
        if (defaultCard) {
          card = defaultCard;
        }
      }
    }

    if (!card) {
      throw new Error('No card specified');
    }

    if (user.balance && user.balance < amount) {
      amount = amount - user.balance;
      user.balance = 0;
      await user.save();
    }

    const client_key = process.env.PAY_MERCHANT;
    const password = process.env.PAY_MERCHANT_PASS;
    const email = ''; // TODO Do we need email?

    const transaction = await PaymentTransaction.create({
      type: 'recurring',
      ids: [req.params.id],
    });
    const md5sum = createHash('md5');
    const reqestString = String(
      strrev(email) + password + strrev(`${card.bin}${card.pan}`)
    ).toUpperCase();

    const hash = md5sum.update(reqestString).digest('hex');

    const payRequest = {
      action: 'RECURRING_SALE',
      client_key,
      order_id: transaction.id,
      order_amount: Number(amount).toFixed(2),
      order_description: `Оплата друку завдання номер ${String(
        task.id
      ).padStart(4, '0')}`,
      recurring_first_trans_id: card.rcId,
      recurring_token: card.rcToken,
      hash,
    };

    logger.info('payrequest ready', payRequest);
    const form = new FormData();
    for (const key in payRequest) {
      if (payRequest.hasOwnProperty(key)) {
        const value = payRequest[key];
        form.append(key, value);
      }
    }

    const formHeaders = form.getHeaders();
    const { data } = await Axios.post(
      'https://secure.platononline.com/post-unq/',
      form,
      {
        headers: {
          ...formHeaders,
        },
      }
    );

    logger.info('platon response' + JSON.stringify(data));

    if (data.result === 'SUCCESS') {
      await task.pay('default_card');
      return res.json(task);
    }

    if (data.result === 'ERROR') {
      return res.status(500).json(data.error_message);
    }

    if (data.result === 'DECLINED') {
      return res.status(500).json(data.decline_reason);
    }

    return res.status(500).json(data.result);
  } catch (error) {
    logger.error(`direct pay error message: ${error.message}`);
    res.status(500).json(error.message);
  }
});

router.patch('/:id/reprint/file', async (req, res) => {
  try {
    const { fileId } = req.body;
    const parentTask = await Task.findByPk(req.params.id);

    const { printConfig, cost, printerId } = parentTask;
    const task = await Task.create({
      printConfig,
      cost,
      pagesQty: 1,
      userId: req.user.id,
      printerId,
      test: true,
    });

    const {
      name,
      originalname,
      encoding,
      mimetype,
      destination,
      filename,
      path,
      size,
      pages,
      userId
    } = await LocalFile.findByPk(fileId);

    const file = await LocalFile.create({
      name,
      originalname,
      encoding,
      mimetype,
      destination,
      filename,
      path,
      size,
      pages,
      userId
    });
    await task.$add('files', [file]);

    return res.json(task);
  } catch (error) {
    logger.error(`Error on reprint: ${error.message}`);
    res.status(500).send(error.message);
  }
});

router.get('/refund', async (req, res) => {
  try {
    const expiredTasks = await Task.findAll({
      where: {
        status: { [Op.in]: ['init', 'payed', 'canceled'] },
        refunded: false,
      },
    });

    logger.info('expired tasks found: ' + expiredTasks.length);
    for (const task of expiredTasks) {
      const updated = await task.expire();
    }
    return res.send('ok')
  } catch (err) {
    logger.error('Error expiring tasks');
    logger.error(err);
  }
});

router.get('/update/files', async (req, res) => {
  try {
    const tasks = await Task.findAll();
    for (const task of tasks) {
      try {
        const file = await LocalFile.findOne({ where: { taskId: task.id } });
        if (file) {
          await file.update({ userId: task.userId });
        }
      } catch ({ message }) {
        logger.error(message);
      }
    }
    res.send('ok');
  } catch ({ message }) {
    logger.error(`Error expiring tasks ${message}`);
    res.status(500).send(message);
  }
});

export const TaskController: Router = router;

function strrev(str) {
  // Reverse a str
  //
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  let ret = '';
  let i = 0;
  for (i = str.length - 1; i >= 0; i--) {
    ret += str.charAt(i);
  }
  return ret;
}
