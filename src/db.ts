import { Model, Op } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';
import { logger } from './utils/logger'; // './server';

const sequelize = new Sequelize({
  database: process.env.DATABASE_NAME,
  dialect: 'postgres',
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASS,
  host: process.env.DATABASE_HOST,
  // storage: path.join(__dirname, '../public/') + 'database.sqlite', // For Testing
  modelPaths: [__dirname + '/models'],
  operatorsAliases: { $like: Op.iLike },
  logging: false
});

export const dbConnect = async () =>
  new Promise<any>((resolve, rej) => {
    sequelize
      .sync() // { force: true }
      .then(res => {
        logger.info('Database sync');
        resolve();
      })
      .catch(err => {
        console.error(err);
        rej(err);
      });
  });

export const getSeqlize = () =>
  sequelize;

export const getModel = (model: string): any => {
  const modelName = model.charAt(0).toUpperCase() + model.slice(1);
  const supplierModel = sequelize.modelManager.getModel(modelName);
  return supplierModel;
};

export { sequelize }