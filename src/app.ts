
require('dotenv').load();

import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as expressJwt from 'express-jwt';
import * as hbs from 'express-hbs';
import * as ExpressSession from 'express-session';
import * as path from 'path';

import { EntityController } from './controllers/entity';
import { PaymentController } from './controllers/payments';
import { PrintController } from './controllers/print';
import { PrinterController } from './controllers/printer';
import { TaskController } from './controllers/task';
import { UserController } from './controllers/user';
import { AdminAuthController } from './controllers/admin/auth';
import { AdminStatisticsController } from './controllers/admin/statistics';
import { AdminUserController } from './controllers/admin/user';
import { CodesController } from './controllers/codes';


const app = express();

app.engine('hbs', hbs.express4({
    partialsDir: path.join(__dirname, '../views/partials'),
    layoutsDir: path.join(__dirname, '../views/layout'),
    defaultLayout: path.join(__dirname, '../views/layout/base.hbs'),
}));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '../views'));

app.locals.HOST = process.env.HOST;
app.locals.FRONTEND = process.env.FRONTEND;

app.use(bodyParser.urlencoded({ extended: true, limit: '50Mb' }));
app.use(bodyParser.json());

app.use(
    ExpressSession({
        secret: process.env.SALT,
        resave: false,
        saveUninitialized: true,
    })
);

// Add CORS
app.all('*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Methods',
        'GET,PUT,POST,DELETE,OPTIONS,PATCH'
    );
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    if (req.method === 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

app.use(
    expressJwt({ secret: process.env.SALT }).unless({
        path: [
            '/user/login',
            '/task/refund',
            '/task/update/files',
            '/user/register',
            '/user/fb',
            '/user/apple',
            '/user/restorePass',
            '/admin/auth/login',
            /^\/print\/*/,
            /\/images\/*/,
            /\/payments\/*/,
            /\/printer\/printFinished\/*/,
            /\/user\/checkPassToken\/*/,
            /\/user\/setNewPass\/*/,
            /\/printer\/state\/*/,
            /\/admin\/user\/file\/*/
        ],
    })
);

// Remove stacktrace from JWT error
app.use((err, req, res, next) => {
    if (err && err.name === 'UnauthorizedError') {
        res.status(err.status).send({ message: err.message });
        return;
    }
    next();
});

app.use('/entity', EntityController);
app.use('/print', PrintController);
app.use('/user', UserController);
app.use('/task', TaskController);
app.use('/printer', PrinterController);
app.use('/payments', PaymentController);
app.use('/admin/auth', AdminAuthController);
app.use('/admin/statistics', AdminStatisticsController);
app.use('/admin/user', AdminUserController);
app.use('/codes', CodesController);

// Static
app.use(express.static(path.join(__dirname, '..', 'assets')));

app.set('port', process.env.PORT || 9000);

export { app };
