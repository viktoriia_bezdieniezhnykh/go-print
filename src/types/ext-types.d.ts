import { Model } from 'sequelize/types';


declare global {
  namespace Express {
    export interface Request {
      entity: any;
      model: any;
      files: any;
      user: any;
    }
  }
}

declare namespace Express {
  export interface Request {
    entity: any;
    model: any;
    files: any;
    user: any;
  }
}
