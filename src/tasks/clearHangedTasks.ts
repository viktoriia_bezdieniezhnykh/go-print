import * as moment from 'moment';
import * as schedule from 'node-schedule';
import { Op } from 'sequelize';

import { Task } from '../models/Task';
import { logger } from '../utils/logger'; // '../server';

export function cleanupHangedTasks() {
  const job = schedule.scheduleJob('*/5 * * * *', async () => {
    const expiredTime = moment().subtract(10, 'minutes').toISOString();
    
    try {
      const expiredTasks = await Task.findAll({
        where: {
          updatedAt: { [Op.lte]: expiredTime },
          status: { [Op.in]: ['printing'] },
        },
      });
      for (const task of expiredTasks) {
        const updated = await task.cancel();
      }
    } catch (err) {
      logger.error(err);
    }
  });

  return job;
}
