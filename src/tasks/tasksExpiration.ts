import * as schedule from 'node-schedule';
import * as moment from 'moment';
import { Task } from '../models/Task';
import { Op } from 'sequelize';
import { logger } from '../utils/logger'; // '../server';

export function expireTasks() {
  const job = schedule.scheduleJob('1 * * * *', async () => {
    const now = moment().minutes(0).seconds(0).milliseconds(0).toISOString();
    const expiredTime = moment(now).add(-24, 'hour').toISOString();

    try {
      const expiredTasks = await Task.findAll({
        where: {
          createdAt: { [Op.lte]: expiredTime },
          status: { [Op.in]: ['init', 'payed'] },
        },
      });


      for (const task of expiredTasks) {
        const updated = await task.expire();
      }

      const archiveTasks = await Task.findAll({
        where: {
          createdAt: { [Op.lte]: expiredTime },
          status: { [Op.in]: ['complete'] },
        },
      });

      for (const task of archiveTasks) {
        task.archived = true;
        await task.save();
      }

    } catch (err) {
      logger.error(err);
    }
  });

  return job;
}
