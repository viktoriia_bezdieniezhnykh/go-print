import * as schedule from 'node-schedule';
import { Op } from 'sequelize';
import { Printer } from '../models/Printer';
import { logger } from '../utils/logger'; // '../server';

export function printersStateUpdate() {
  const job = schedule.scheduleJob('printersStateUpdate', '* * * * *', async function() {
    try {
      const printers = await Printer.findAll({where: {status: {[Op.ne]: 'stopped'}}});
      for (const printer of printers) {
        await printer.checkState();
      }
    } catch (err) {
      logger.error(err);
    }
  });

  return job;
}
