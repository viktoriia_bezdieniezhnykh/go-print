export const prices = {
  color: {
    oneside: 5,
    double: 7
  },
  bw: {
    oneside: 1.5,
    double: 2
  }
};
