import { Namespace, Server, Socket } from 'socket.io';
import { logger } from '../utils/logger'; // '../server';

export class TaskEvents {
  public nsps: Namespace;
  public socket: Socket;

  constructor(private readonly _io: Server) {
    this.nsps = this._io.of(`/tasks`);

    this.nsps.on('connection', (socket: any) => {
      logger.info('Socket connected');
      this.socket = socket;

      const query = socket.handshake.query;
      const taskId = query.taskId;

      logger.info(`Socket for id ${taskId}`);

      if (taskId) {
        this.socket.join(taskId);
      }

      this.listen();
    });
  }

  public listen(): void {
    this.socket.on('disconnect', () => this.disconnect());
  }

  public taskComplete(taskId) {
    logger.info(`comlete task ${taskId}`);
    this.nsps.to(taskId).emit('taskComplete');
  }

  // this emit on error
  public taskCompleteWithErrors(taskId) {
    this.nsps.to(taskId).emit('taskCompleteWithErrors');
  }

  // for manual cancel
  public taskCanceled(taskId) {
    this.nsps.to(taskId).emit('taskCanceled');
  }

  public disconnect(): void {
    logger.info('Socket disconnected');
  }
}
