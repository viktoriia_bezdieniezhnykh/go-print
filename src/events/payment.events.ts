import { Namespace, Server, Socket } from 'socket.io';
import { logger } from '../utils/logger'; // '../server';

export class PaymentEvents {
  public nsps: Namespace;
  public socket: Socket;

  constructor(private readonly _io: Server) {
    this.nsps = this._io.of(`/payments`);

    this.nsps.on('connection', (socket: any) => {
      logger.info('Socket connected');
      this.socket = socket;

      const query = socket.handshake.query;
      const taskId = query.taskId;

      logger.info(`Payment socket for id ${taskId}`);

      if (taskId) {
        this.socket.join(taskId);
      }

      this.listen();
    });
  }

  public listen(): void {
    this.socket.on('disconnect', () => this.disconnect());
  }

  public paymentSucceed(taskId, payload) {
    this.nsps.to(taskId).emit('paymentSucceed', payload);
  }

  public paymentCanceled(taskId) {
    this.nsps.to(taskId).emit('paymentCanceled');
  }

  public disconnect(): void {
    logger.info('Socket disconnected');
  }
}
