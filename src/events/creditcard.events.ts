import { Namespace, Server, Socket } from 'socket.io';
import { logger } from '../utils/logger'; // '../server';

export class CCardEvents {
  public nsps: Namespace;
  public socket: Socket;

  constructor(private readonly _io: Server) {
    this.nsps = this._io.of(`/creditcards`);

    this.nsps.on('connection', (socket: any) => {
      logger.info('Socket connected');
      this.socket = socket;

      const query = socket.handshake.query;
      const userId = query.userId;

      logger.info(`Socket for user id ${userId}`,);

      if (userId) {
        this.socket.join(userId);
      }

      this.listen();
    });
  }

  public listen(): void {
    this.socket.on('disconnect', () => this.disconnect());
  }

  public cardApproved(userId, payload) {
    this.nsps.to(userId).emit('cardApproved', payload);
  }

  public disconnect(): void {
    logger.info('Socket disconnected');
  }
}
