import { Model } from "sequelize/types";

export const updateOrCreate = async (model, where, data) => {
  const current: Model = await model.findOne({ where });
  if (!current) {
    const item= await model.create(data);
    return { item, created: true };
  }
  const item = await current.update(data);
  return { item, created: false };
};
