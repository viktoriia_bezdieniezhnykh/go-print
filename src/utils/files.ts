import Axios from 'axios';
import * as child from 'child_process';
import * as util from 'util';

import * as pngToJpeg from 'png-to-jpeg';
import FormData = require('form-data');
import {
  createReadStream,
  createWriteStream,
  readFileSync,
  rename,
  renameSync,
  statSync,
  unlinkSync,
  writeFileSync,
} from 'fs';
import * as mime from 'mime-types';
import moment = require('moment');
import { join, parse } from 'path';
import * as pdfkit from 'pdfkit';

import { LocalFile } from '../models/LocalFile';
import { logger } from './logger'; // '../server';

const publicDir = join(__dirname, '../..', 'public/temp');

export const countPages = async (files) => {
  let pages = 0;
  for (const file of files) {
    if (/[\/.](gif|jpg|jpeg|tiff|png)$/i.test(String(file.mimetype))) {
      pages++;
      continue;
    }
    try {
      const fileData = readFileSync(file.path);
      const response = await Axios.put('http://tika:9998/meta', fileData, {
        headers: {
          'Content-Type': 'application/octet-stream',
          Accept: 'application/json',
        },
        maxContentLength: 100 * 1024 * 1024,
      });

      if (response.data['xmpTPg:NPages']) {
        pages = pages + Number(response.data['xmpTPg:NPages']);
      } else {
        pages++;
      }
    } catch (error) {
      logger.error(error);
      throw new Error('Error on page count');
      // pages++;
    }
  }
  return pages;
};

export const extractFileData = async (file) => {
  try {
    const fileData = readFileSync(file.path);
    const { data } = await Axios.put('http://tika:9998/meta', fileData, {
      headers: {
        'Content-Type': 'application/octet-stream',
        Accept: 'application/json',
      },
      maxContentLength: 100 * 1024 * 1024,
    });

    if (data['xmpTPg:NPages']) {
      file.pages = Number(data['xmpTPg:NPages']);
    } else {
      file.pages = 1;
    }
    file.mimetype = data['Content-Type'];

    if (!/(?:\.([^.]+))?$/.exec(file.filename)[1]) {
      const ext = mime.extension(file.mimetype);
      file.filename = `${file.filename}.${ext}`;

      const dir = parse(file.path).dir;
      const newPath = `${dir}/${file.filename}`;
      renameSync(file.path, newPath);
      file.path = newPath;
    }

    if (String(file.filename).toLowerCase().endsWith('.jpeg')) {
      const onExtFilename = String(file.filename)
        .split('.')
        .slice(0, -1)
        .join('.');
      file.filename = `${onExtFilename}.jpg`

      const dir = parse(file.path).dir;
      const newPath = `${dir}/${file.filename}`;
      renameSync(file.path, newPath);
      file.path = newPath;
    }

    return file;
  } catch ({ message }) {
    logger.error(`Extract data error: ${message}`);
    return file;
  }
};

export const convertFile = async (file: LocalFile): Promise<LocalFile> =>
  new Promise((resolve, reject) => {
    const fileStream = createReadStream(file.path);

    const formData = new FormData();
    formData.append('file', fileStream);

    const originalFile = file.path;

    const onExtFilename = String(file.filename)
      .split('.')
      .slice(0, -1)
      .join('.');
    const newFilename = `${onExtFilename}.pdf`;
    const convertedFile = `${publicDir}/${newFilename}`;
    const convertedStream = createWriteStream(convertedFile);

    convertedStream.on('finish', () => {
      convertedStream.close();
      const stat = statSync(convertedFile);
      file.path = convertedFile;
      file.mimetype = 'application/pdf';
      file.filename = newFilename;
      file.size = String(stat.size);
      unlinkSync(originalFile);

      if (file instanceof LocalFile) {
        file.save().then(resolve).catch(reject);
      } else {
        resolve(file);
      }
    });

    convertedStream.on('error', (err) => {
      convertedStream.close();
      unlinkSync(convertedFile);
      return reject(err);
    });

    Axios.post('http://unoconv:3000/unoconv/pdf', formData, {
      headers: formData.getHeaders(),
      responseType: 'stream',
    })
      .then((response) => {
        response.data.pipe(convertedStream);
      })
      .catch(reject);
  });

export const convertFileGoten = async (file: LocalFile): Promise<LocalFile> =>
  new Promise((resolve, reject) => {
    const fileStream = createReadStream(file.path);

    const formData = new FormData();
    formData.append('files', fileStream);

    const originalFile = file.path;

    const onExtFilename = String(file.filename)
      .split('.')
      .slice(0, -1)
      .join('.');
    const newFilename = `${onExtFilename}.pdf`;
    const convertedFile = `${publicDir}/${newFilename}`;
    const convertedStream = createWriteStream(convertedFile);

    convertedStream.on('finish', () => {
      convertedStream.close();
      const stat = statSync(convertedFile);
      file.path = convertedFile;
      file.mimetype = 'application/pdf';
      file.filename = newFilename;
      file.size = String(stat.size);
      unlinkSync(originalFile);

      if (file instanceof LocalFile) {
        file.save().then(resolve).catch(reject);
      } else {
        resolve(file);
      }
    });

    convertedStream.on('error', (err) => {
      convertedStream.close();
      unlinkSync(convertedFile);
      return reject(err);
    });

    Axios.post('http://gotenberg:3000/convert/office', formData, {
      headers: formData.getHeaders(),
      responseType: 'stream',
    })
      .then((response) => {
        response.data.pipe(convertedStream);
      })
      .catch(reject);
  });

export const combineImagesToPdf = (files) =>
  new Promise((resolve, reject) => {
    const date = moment().unix();
    const doc = new pdfkit({
      autoFirstPage: false,
    });
    const newFilename = `imgs-${date}.pdf`;
    const convertedFile = `${publicDir}/${newFilename}`;
    const convertedStream = createWriteStream(convertedFile);
    doc.pipe(convertedStream);

    convertedStream.on('finish', () => {
      convertedStream.close();
      const stat = statSync(convertedFile);
      const file: any = {};
      file.path = convertedFile;
      file.mimetype = 'application/pdf';
      file.filename = newFilename;
      file.size = String(stat.size);

      resolve(file);
      // LocalFile.create(file).then(resolve).catch(reject);
    });

    for (const image of files) {
      const img = doc.openImage(image.path);
      doc.addPage({ size: 'A4' });
      doc.image(img, 20, 20, {
        fit: [doc.page.width - 40, doc.page.height - 40],
        align: 'center',
        valign: 'center',
      });
    }
    doc.end();
  });

export const fitPdfToPage = async (file: LocalFile) => {
  try {
    const { exec } = child;
    const execPromise = util.promisify(exec);

    const convertedFile = `${publicDir}/fitted-${file.filename}`;
    const { stdout, stderr } = await execPromise(
      ['pdfposter', `-p1x1A4`, file.path, convertedFile].join(' ')
    );

    file.path = convertedFile;
    file.filename = `fitted-${file.filename}`;

    return file;
  } catch (error) {
    logger.error(error);
    return file;
  }
};


export const resavePdf = async (file: LocalFile) => {
  try {
    const { PDFDocument } = require('pdf-lib');
    const fs = require('fs');

    const content = await PDFDocument.load(fs.readFileSync(file.path));

    // Create a new document
    const doc = await PDFDocument.create();

    const contentPages = await doc.copyPages(content, content.getPageIndices());
    for (const page of contentPages) {
      doc.addPage(page);
    }

    // Write the PDF to a file
    fs.writeFileSync(file.path, await doc.save());

    return file;
  } catch (error) {
    logger.error(error);
    return file;
  }
};


export const convertToJpeg = async (file: LocalFile) => {
  let buffer = readFileSync(file.path);

  const onExtFilename = String(file.filename)
    .split('.')
    .slice(0, -1)
    .join('.');

  const output = await pngToJpeg({ quality: 100 })(buffer);

  const newFilename = `${onExtFilename}.jpg`;
  const convertedFile = `${publicDir}/${newFilename}`;
  writeFileSync(convertedFile, output);

  const stat = statSync(convertedFile);
  file.path = convertedFile;
  file.mimetype = 'image/jpeg';
  file.filename = newFilename;
  file.size = String(stat.size);

  return file;
}