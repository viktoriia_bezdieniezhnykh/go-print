import * as jwt from 'jsonwebtoken';
const crypto = require('crypto');

import { User } from '../models/User';
import { safeAttributes } from '../data/safeAttributes';
import { CreditCard } from '../models/CreditCard';
import { logger } from '../utils/logger';

export const authUser = async ({ email, password }) => {
    const user = await User.findOne({
        where: { email },
        attributes: ['hash', ...safeAttributes],
        include: [CreditCard],
    });

    if (!user) {
        throw new Error("User not found");
    }

    const hash = crypto
        .createHash('md5')
        .update(`${password}|${process.env.SALT}|pass`)
        .digest('hex');

    if (user.hash !== hash) {
        throw new Error('Password is incorrect.');
    }

    user.lastSeen = new Date();
    await user.save();

    const u = user.toJSON();
    delete u['hash'];
    delete u['createdAt'];
    delete u['updatedAt'];

    const expire = Math.floor(Date.now() / 1000) + 60 * 43200;
    const token = jwt.sign({ id: user.id, exp: expire }, process.env.SALT);

    return { token, expire, user };
}

export const createUser = async (payload) => {
    const { email, password } = payload;
    const exist = await User.findOne({ where: { email } });
    if (exist) {
        throw new Error("User exist");        
    }

    const hash = crypto
        .createHash('md5')
        .update(`${password}|${process.env.SALT}|pass`)
        .digest('hex');

    payload.hash = hash;

    const user = await User.create(payload);
    const { id, name, lastSeen } = user;
    const expire = Math.floor(Date.now() / 1000) + 60 * 43200;
    const token = jwt.sign({ id: user.id, exp: expire }, process.env.SALT);

    const u = user.toJSON();
    delete u['hash'];
    delete u['createdAt'];
    delete u['updatedAt'];

    return { token, expire, user };
}