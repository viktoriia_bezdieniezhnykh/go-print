import * as soap from 'soap';
import * as Cookie from 'soap-cookie';
import { get } from 'lodash';

export const sendSMS = async ({ sender, phone, text }) => {
  return new Promise((res, rej) => {
    if (process.env.SMS_BLOCKED === 'true') {
      return;
    }
    if (!phone) {
      return;
    }
    soap.createClient('http://turbosms.in.ua/api/wsdl.html', (err, client) => {
      if (err) {
        console.error(err);
        rej(err);
        return;
      }
      // Auth data
      const auth = {
        login: process.env.TURBOSMS_LOGIN,
        password: process.env.TURBOSMS_PASSWORD
      };
      client.Auth(auth, (err, result) => {
        if (err) {
          console.error(err);
          rej(err);
          return;
        }
        client.setSecurity(new Cookie(client.lastResponseHeaders));
  
        phone = phone.replace(/[^0-9]/gim, '');
        const sms = {
          sender,
          destination: `+${phone}`,
          text
        };
        
        client.SendSMS(sms, (err, result) => {
          if (err) {
            console.error(err);
            rej(err);
            return;
          }
          if (get(result, 'SendSMSResult.ResultArray[0]') === 'Вы не авторизированы') {
            return rej(new Error('TurboSMS Auth error'));
          }
          if (get(result, 'SendSMSResult.ResultArray[0]') === 'Данная подпись запрещена администратором') {
            return rej(new Error('TurboSMS sign error'));
          }
          if (get(result, 'SendSMSResult.ResultArray[0]') === 'Не хватает 1 кредитов для отправки SMS') {
            return rej(new Error('TurboSMS balance error'));
          }
          res('ok')
        });
      });
    });
  })
};

