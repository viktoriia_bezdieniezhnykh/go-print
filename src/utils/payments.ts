import { PaymentTransaction } from '../models/PaymentTransaction';
import { createHash } from 'crypto';
import { logger } from './logger'; // '../server';

/**
 * Create new pay transaction
 *
 * @param data Date for payment
 * @param type Transaction type
 */
export const createTransaction = async (data, type = 'application') => {
  const transaction = await PaymentTransaction.create(data);
};

export const updateTransaction = data => { };

function escapeUnicode(str) {
  return str.replace(/[\u00A0-\uffff]/gu, function (c) {
    return "\\u" + ("000" + c.charCodeAt().toString(16)).slice(-4)
  });
}

export const generateForm = ({ orderId, detailsData = {}, type = null }) => {
  const domain = process.env.FRONTEND;
  let key = process.env.PAY_MERCHANT;
  let password = process.env.PAY_MERCHANT_PASS;
  const payment = 'CC';
  let url = `${domain}/success/payment`;
  let formid;
  let req_token;
  let ext;

  const paymentData: any = {
    amount: Number(detailsData['amount']).toFixed(2),
    description: `Оплата друку завдання номер ${String(detailsData['taskId']).padStart(4, '0')}`,
    currency: 'UAH',
    recurring: 'Y'
  };

  req_token = 'Y';

  if (type === 'verification') {
    url = `${domain}/success/verification`;
    formid = 'verify';
    req_token = 'Y';
    key = process.env.PAY_MERCHANT;
    password = process.env.PAY_MERCHANT_PASS;
    paymentData.recurring = 'Y';
    paymentData.description = `Card Verification`;
    paymentData.amount = '0.73';
    ext = {};
  }

  logger.info(JSON.stringify(paymentData));

  const data = Buffer.from(JSON.stringify(paymentData)).toString('base64');

  const md5sum = createHash('md5');
  const reqestString = String(
    `${strrev(key)}${strrev(payment)}${strrev(data)}${strrev(url)}${strrev(
      password
    )}`
  ).toUpperCase();

  const sign = md5sum.update(reqestString).digest('hex');
  const order = orderId;

  logger.info('payment output data', {
    payment,
    key,
    url,
    data,
    req_token,
    formid,
    sign,
    order,
    ...ext,
  });

  return {
    payment,
    key,
    url,
    data,
    req_token,
    formid,
    sign,
    order,
    ...ext,
  };
};

export function strrev(str) {
  // Reverse a str
  //
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  let ret = '';
  let i = 0;
  for (i = str.length - 1; i >= 0; i--) {
    ret += str.charAt(i);
  }
  return ret;
}
