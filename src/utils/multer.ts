import * as fs from 'fs';
import * as mime from 'mime-types';
import * as multer from 'multer';
import * as path from 'path';

import { mkDirSyncRecursive } from './recursive-dir-sync';

const publicDir = path.join(__dirname, '../..', 'public');
const tempDir = path.join(__dirname, '../..', 'public/temp');

if (!fs.existsSync(publicDir)) {
  mkDirSyncRecursive(publicDir);
}
if (!fs.existsSync(tempDir)) {
  mkDirSyncRecursive(tempDir);
}

const storage = multer.diskStorage({
  destination: tempDir,
  filename: (req, file, cb) => {
    // let ext: string;
    
    // ext = mime.extension(file.mimetype);
    // cb(null, `file-${Date.now()}.${file.name}`);
    if (file.originalname.length > 45) {
      file.originalname = String(file.originalname).slice(file.originalname.length - 45);
    }
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

export const upload = multer({ storage });
