import * as request from 'request';
// import {get} from 'lodash';

export const getPrinterUrls = async () => {
  const CUPSurl = `http://${process.env.CUPS_SERVER}/printers`;
  const printersUrls = [];

  return new Promise<string[]>((resolve, reject) => {
    request(CUPSurl, (error, response, body) => {
      if (error) {
        return reject(error);
      }

      if (!error && response.statusCode === 200) {
        const printersMatches = body.match(
          /<A HREF="\/printers\/([a-zA-Z0-9-_^"]+)">/gm
        );

        let i;

        if (printersMatches) {
          for (i = 0; i < printersMatches.length; i++) {
            const a = /"\/printers\/([a-zA-Z0-9-^_"]+)"/.exec(
              printersMatches[i]
            );
            if (a) {
              printersUrls.push(CUPSurl + '/' + a[1]);
            }
          }
        }
      }
      resolve(printersUrls);
    });
  });
};

export const getPrinterAttributes = async (
  printer,
  requestedAttributes = [
    'queued-job-count',
    'printer-input-tray',
    'marker-names',
    'marker-levels',
    'printer-state',
    'printer-state-reasons',
    'printer-up-time',
    'printer-state-message',
    'printer-input-tray'
  ]
) =>
  new Promise((resolve, reject) => {
    printer.execute(
      'Get-Printer-Attributes',
      {
        'operation-attributes-tag': {
          'requested-attributes': requestedAttributes,
        },
      },
      (err, printerStatus) => {
        if (err) {
          return reject(err);
        }
        resolve(printerStatus);
      }
    );
  });

export const getPrinterJobs = async printer =>
  new Promise((resolve, reject) => {
    printer.execute(
      'Get-Jobs',
      {
        'operation-attributes-tag': {
          // "limit": 10,
          "which-jobs": "completed",
        },
      },
      (err, printerStatus) => {
        if (err) {
          return reject(err);
        }
        resolve(printerStatus);
      }
    );
  });

export const validateJob = async printer =>
  new Promise((resolve, reject) => {
    printer.execute(
      'Get-Jobs',
      {
        'operation-attributes-tag': {},
      },
      (err, printerStatus) => {
        if (err) {
          return reject(err);
        }
        resolve(printerStatus);
      }
    );
  });

/*'job-creation-attributes-supported':
 [ 'copies',
   'finishings',
   'ipp-attribute-fidelity',
   'job-hold-until',
   'job-name',
   'job-priority',
   'job-sheets',
   'media',
   'media-col',
   'multiple-document-handling',
   'number-up',  [ 1, 2, 4, 6, 9, 16 ]
   'output-bin',
   'orientation-requested', [ 'portrait', 'landscape', 'reverse-landscape', 'reverse-portrait' ]
   'page-ranges',
   'print-color-mode', [ 'monochrome', 'color' ]
   'print-quality',
   'printer-resolution',
   'sides'  [ 'one-sided', 'two-sided-long-edge', 'two-sided-short-edge' ]
  ],*/

export const sendDataToPrinter = async (
  printer,
  data,
  jobAttr = {},
  documentFormat = 'application/pdf',
  jobName = 'documentPrinting',
  documentName = 'doc.pdf'
) =>
  new Promise((resolve, reject) => {
    printer.execute(
      'Print-Job',
      {
        'operation-attributes-tag': {
          'requesting-user-name': 'goprint',
          'job-name': jobName,
          'document-format': documentFormat,
          'document-name': documentName
        },
        'job-attributes-tag': jobAttr,
        data,
      },
      (err, res) => {
        if (err) {
          return reject(err);
        }
        resolve(res);
      }
    );
    // getPrinterAttributes(printer)
    //   .then(status => {    //     

    //     if (status['printer-attributes-tag']['printer-state'] === 'idle') {
    //       printer.execute(
    //         'Print-Job',
    //         {
    //           'operation-attributes-tag': {
    //             'requesting-user-name': 'goprint',
    //             'job-name': 'documentPrinting',
    //           },
    //           'job-attributes-tag': jobAttr,
    //           data,
    //         },
    //         (err, res) => {
    //           if (err) {
    //             return reject(err);
    //           }
    //           resolve(res);
    //         }
    //       );
    //     } else {
    //       const cupsMessage =
    //         status['printer-attributes-tag']['printer-state-message'];
    //       reject(new Error(`Printer not ready, CUPS say: ${cupsMessage}`));
    //     }
    //   })
    //   .catch(err => {
    //     reject(err);
    //   });
  });

export const getJobAttributes = async (
  printer,
  jobId,
  requestedAttributes = [
    'job-state',
    'job-state-reasons',
    'job-media-progress',
    'date-time-at-creation',
    'document-format-supplied',
    'job-media-sheets-completed',
    'job-impressions-completed'
  ]
) =>
  new Promise((resolve, reject) => {
    printer.execute(
      'Get-Job-Attributes',
      {
        'operation-attributes-tag': {
          'job-id': jobId,
          'requested-attributes': requestedAttributes,
        },
      },
      (err, job) => {
        if (err) {
          return reject(err);
        }
        resolve(job);
      }
    );
  });

export const watchPrintJob = async (printer, job, tries = 30) =>
  new Promise((resolve, reject) => {
    let counter = 0;
    let time = new Date().getTime();
    const interval = setInterval(() => {
      const jobId = job['job-attributes-tag']['job-id'];
      getJobAttributes(printer, jobId)
        .then(jobAttributes => {

          if (jobAttributes) {
            console.info(
              `${jobId} -- job-state ${jobAttributes['job-attributes-tag']['job-state']}`
            );

            switch (jobAttributes['job-attributes-tag']['job-state']) {
              case 'completed':
                clearInterval(interval);
                resolve('complete');
                break;
              case 'processing-stopped':
                clearInterval(interval);
                reject(new Error('processing-stopped'));
                break;
              case 'canceled':
                clearInterval(interval);
                reject(new Error('canceled'));
                break;
              default:
                break;
            }
          }

          if (new Date().getTime() > time + 300000) {
            const jobId = job['job-attributes-tag']['job-id'];
            cancelJob(printer, jobId)
              .then(res => {
                clearInterval(interval);
                reject(new Error('Job is canceled - timeout'));
              })
              .catch(err => {
                clearInterval(interval);
                reject(err);
              });
          }

          // if (counter === tries) {
          //   const jobId = job['job-attributes-tag']['job-id'];
          //   cancelJob(printer, jobId)
          //     .then(res => {
          //       clearInterval(interval);
          //       reject(new Error('Job is canceled - too many tries'));
          //     })
          //     .catch(err => {
          //       clearInterval(interval);
          //       reject(err);
          //     });
          // }
          counter += 1;
        })
        .catch(err => {
          reject(err);
        });
    }, 5000);
  });

export const cancelJob = async (printer, jobId) =>
  new Promise((resolve, reject) => {
    printer.execute(
      'Cancel-Job',
      {
        'operation-attributes-tag': {
          'attributes-charset': 'utf-8',
          'printer-uri': printer.uri,
          'job-id': jobId,
          'requesting-user-name': 'goprint',
        },
      },
      (err, res) => {
        if (err) {
          return reject(err);
        }
        resolve(res);
      }
    );
  });

const attrData = {
  color: {
    attrName: 'print-color-mode',
    optionsMatch: {
      color: 'color',
      bw: 'monochrome',
    },
  },
  print: {
    attrName: 'sides',
    optionsMatch: {
      oneside: 'one-sided',
      double: 'two-sided-long-edge',
    },
  },
  orientation: {
    attrName: 'orientation-requested',
    optionsMatch: {
      portrait: 'portrait',
      landscape: 'landscape',
    },
  },
  onPage: {
    attrName: 'number-up',
    optionsMatch: {
      onePerPage: 1,
      twoPerPage: 2,
      fourPerPage: 4,
    },
  },
};

export const attributesFromTask = config => {
  // const config = task.printConfig;

  //const config = JSON.parse(
  //  '{"color": "bw", "print": "oneside", "orientation": "portrait", "onPage": "onePerPage"}'
  //);

  const ippAttrs = {};
  for (const attr in config) {
    if (config.hasOwnProperty(attr)) {
      const value = config[attr];

      if (attrData[attr]) {
        const ippAttr = attrData[attr];
        if (ippAttr.optionsMatch[value]) {
          ippAttrs[ippAttr.attrName] = ippAttr.optionsMatch[value];
        }
      }
    }
  }
  return ippAttrs;
};

export const printOn = async (printer, bufferToBePrinted) =>
  sendDataToPrinter(printer, bufferToBePrinted);
