import { createLogger, format, transports } from 'winston';
const newrelicFormatter = require('@newrelic/winston-enricher')

export const logger = createLogger({
  level: 'info',
  format: format.combine(
    format.json(),
    newrelicFormatter()
  ),
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new transports.File({ filename: 'logs/error.log', level: 'error' }),
    new transports.File({ filename: 'logs/combined.log' }),
  ],
});

if (!process.env.APP_TEST) {
  logger.add(new transports.Console({
    format: format.prettyPrint(),
  }));
}
