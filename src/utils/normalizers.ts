import { parsePhoneNumberFromString } from 'libphonenumber-js';
import { logger } from './logger'; // '../server';

export const phoneNormalizer = (req, res, next) => {
  try {
    const phoneFields = ['secondPhone', 'phone'];
    for (const phoneField of phoneFields) {
      if (typeof req.body[phoneField] !== 'undefined') {
        let phoneCode: any = 'UA';
        if (typeof req.body.phoneCode !== 'undefined') {
          phoneCode = String(req.body.phoneCode).toUpperCase();
        }
        const phoneNumber = parsePhoneNumberFromString(`${req.body[phoneField]}`, phoneCode);
        if (phoneNumber.isValid()) {
          req.body[phoneField] = phoneNumber.number;
        } else {
          req.body[phoneField] = null;
        }
      }
    }
  } catch (error) {
    logger.error(error);
    req.body['phone'] = null;
  }
  next();
};
