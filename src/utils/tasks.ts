import { printersStateUpdate } from '../tasks/printersStateUpdate';
import { expireTasks } from '../tasks/tasksExpiration';
import { cleanupHangedTasks } from '../tasks/clearHangedTasks';

export const setTasks = async () => {
    return [expireTasks(),
    printersStateUpdate(),
    cleanupHangedTasks()];
}