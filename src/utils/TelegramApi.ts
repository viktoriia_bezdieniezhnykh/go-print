import Axios from "axios";

export const setWebhook = async (token, webhook_url) => {
  const { data } = await Axios.get(
    `https://api.telegram.org/bot${token}/setWebhook`,
    {
      params: {
        url: webhook_url
      }
    }
  );
  return data;
};

export const sendMessage = async (token, {chat_id, text}) => {
  const { data } = await Axios.get(
    `https://api.telegram.org/bot${token}/sendMessage`,
    {
      params: {
        chat_id,
        text
      }
    }
  );
  return data;
};

// Interfaces

export interface TelegramUpdate {
  update_id: number;
  message?: TelegramMessage;
  edited_message?: TelegramMessage;
  channel_post?: TelegramMessage;
  edited_channel_post?: TelegramMessage;
  inline_query?: any;
  chosen_inline_result?: any;
  callback_query?: any;
  shipping_query?: any;
  pre_checkout_query?: any;
  poll?: any;
  poll_answer?: any;
}

export interface TelegramMessage {
  message_id: number;
  from?: any;
  date: number;
  chat: any;
  forward_from?: any;
  forward_from_chat?: any;
  forward_from_message_id?: number;
  forward_signature?: string;
  forward_sender_name?: string;
  forward_date?: number;
  reply_to_message?: TelegramMessage;
  edit_date?: number;
  media_group_id?: string;
  author_signature?: string;
  text?: string;
  entities?: any[];
  caption_entities?: any[];
  audio?: any;
  document?: any;
  animation?: any;
  game?: any;
  photo?: any[];
  sticker?: any;
  video?: any;
  voice?: any;
  video_note?: any;
  caption?: string;
  contact?: any;
  location?: any;
  venue?: any;
  poll?: any;
  new_chat_members?: any[];
  left_chat_member?: any;
  new_chat_title?: string;
  new_chat_photo?: any[];
  delete_chat_photo?: boolean;
  group_chat_created?: boolean;
  supergroup_chat_created?: boolean;
  channel_chat_created?: any;
  migrate_to_chat_id?: number;
  migrate_from_chat_id?: number;
  pinned_message?: TelegramMessage;
  invoice?: any;
  successful_payment?: any;
  connected_website?: any;
  passport_data?: any;
  reply_markup?: any;
}
