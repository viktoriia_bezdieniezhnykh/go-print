import * as juice from 'juice';
import * as nodemailer from 'nodemailer';

import { app } from '../app';
import { logger } from './logger'; // '../server';

export class EmailSender {
  // public auth = {
  //   auth: {
  //     api_key: 'a4f45d848204300f657e6d28b47060b6-f877bd7a-aee1ef47',
  //     domain: 'mg.xror.net'
  //   }
  // };

  // public transporter = nodemailer.createTransport(mg(this.auth));

  public readonly transporter = nodemailer.createTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    auth: {
      user: 'postmaster@xror.net',
      pass: '0ec55837c900c99233201746a392a9b0-6f4beb0a-a0b127fa',
    },
  });

  public async sendEmail({
    from = { name: 'GoPrint', address: 'no-reply@xror.net' },
    to = 'xrors.mail@gmail.com',
    subject = 'Email',
    template = 'newUser',
    variables = {},
  }: {
    from?: any;
    to?: string;
    subject?: string;
    template?: string;
    variables?: any;
  } = {}) {
    if (process.env.EMAIL_BLOCKED) {
      return;
    }
    // tslint:disable-next-line: no-inferred-empty-object-type
    return new Promise((resolve, reject) => {
      app.render(template, variables, (err, data) => {
        if (err) {
          logger.error(err);
          reject(err);
        } else {
          const mailOptions = {
            from,
            to,
            subject,
            html: juice(data),
          };
          this.transporter.sendMail(mailOptions, (err, info) => {
            if (err !== null) {
              logger.error(err);
              reject(err);
            } else {
              logger.error('Message sent: ' + info.response);
              resolve(info);
            }
          });
        }
      });
    });
  }
}
